package bo.com.salazargonzalo.module.todosport.app.sale.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class SaleNotFoundException extends RuntimeException {
}
