package bo.com.salazargonzalo.module.todosport.app.notification.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class NotificationNotFoundException extends RuntimeException {
}
