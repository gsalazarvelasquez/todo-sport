package bo.com.salazargonzalo.module.todosport.app.saledetail.model;

import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;
import bo.com.salazargonzalo.module.todosport.app.shoe.model.Shoe;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "SALES_DETAIL")
@Getter
@Setter
@ToString
public class SaleDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SALE_DETAIL_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "SALE_ID", foreignKey = @ForeignKey(name = "fk_sales_detail_sale_id"))
    private Sale sale;

    @ManyToOne
    @JoinColumn(name = "SHOE_ID", foreignKey = @ForeignKey(name = "fk_sales_detail_shoe_id"))
    private Shoe shoe;

    @Column(name = "PAR")
    private Integer par;

    @Column(name = "DOZEN")
    private String dozen;

    @Column(name = "PRICE")
    private String price;

    @Column(name = "SHOE_SIZE")
    private String shoeSize;

    public SaleDetail() {
    }
}
