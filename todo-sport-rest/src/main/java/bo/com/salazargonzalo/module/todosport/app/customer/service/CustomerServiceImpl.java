package bo.com.salazargonzalo.module.todosport.app.customer.service;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.todosport.app.customer.exception.CustomerNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.customer.model.Customer;
import bo.com.salazargonzalo.module.todosport.app.customer.repository.CustomerRepository;
import bo.com.salazargonzalo.module.todosport.service.ICustomerService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class CustomerServiceImpl implements ICustomerService {

    @Inject
    private CustomerRepository customerRepository;

    @Override
    public Customer add(Customer customer) {
        customer.setStatus(StatusEnum.ENABLED);
        return customerRepository.save(customer);
    }

    @Override
    public void update(Customer customer) throws CustomerNotFoundException {
        customerRepository.update(customer);
    }

    @Override
    public void delete(Long id) throws CustomerNotFoundException {
        Customer customer = findById(id);
        customer.setStatus(StatusEnum.DELETED);
        update(customer);
    }

    @Override
    public Customer findById(Long id) throws CustomerNotFoundException {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isAbsent()) {
            throw new CustomerNotFoundException();
        }
        return customerOptional.get();
    }

    @Override
    public List<Customer> getAll() {
        return customerRepository.findAllEnabled();
    }
}
