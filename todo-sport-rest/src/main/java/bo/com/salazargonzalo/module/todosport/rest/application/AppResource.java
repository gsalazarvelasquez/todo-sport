package bo.com.salazargonzalo.module.todosport.rest.application;

import bo.com.salazargonzalo.module.shared.rest.security.annotations.LoggedIn;
import bo.com.salazargonzalo.module.todosport.service.ApplicationService;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("App")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Stateless
public class AppResource {

    @Inject
    private Logger log;

    @Inject
    public ApplicationService applicationService;

    @POST
    @LoggedIn
    @Path("/reset")
    public Response resetSystemData() {
        try {
            applicationService.resetData();
            return Response
                    .ok()
                    .entity("system reset!")
                    .build();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @GET
    @LoggedIn
    @Path("/test")
    public Response testResponse() {
        try {
            return Response
                    .ok()
                    .entity("test")
                    .build();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }
}