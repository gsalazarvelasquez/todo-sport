package bo.com.salazargonzalo.module.todosport.app.shoesize.repository;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.shared.utils.LongUtil;
import bo.com.salazargonzalo.module.todosport.app.shoesize.model.ShoeSize;

import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class ShoeSizeRepository extends Repository {

    public List<ShoeSize> findAll() {
        return findAll(ShoeSize.class);
    }

    public List<ShoeSize> findAllEnabled() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("status", StatusEnum.ENABLED);
        return findBy(ShoeSize.class, filters);
    }

    public Optional<ShoeSize> getById(final Long id) {
        return LongUtil.isEmpty(id) ? Optional.absent() : getById(ShoeSize.class, id);
    }
}
