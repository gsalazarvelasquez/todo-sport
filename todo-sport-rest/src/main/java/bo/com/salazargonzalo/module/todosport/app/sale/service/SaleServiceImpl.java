package bo.com.salazargonzalo.module.todosport.app.sale.service;

import bo.com.salazargonzalo.domain.enumerator.SaleStatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;
import bo.com.salazargonzalo.module.todosport.app.sale.repository.SaleRepository;
import bo.com.salazargonzalo.module.todosport.service.ISaleService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
public class SaleServiceImpl implements ISaleService {

    @Inject
    private SaleRepository saleRepository;

    @Override
    public List<Sale> findAll() {
        return saleRepository.findAll();
    }

    @Override
    public Sale add(Sale sale) {
        sale.setStatus(SaleStatusEnum.PAID);
        return saleRepository.save(sale);
    }

    @Override
    public void update(Sale sale) throws SaleNotFoundException {
        saleRepository.update(sale);
    }

    @Override
    public void remove(Long id) throws SaleNotFoundException {
        Sale sale = getById(id);
        sale.setStatus(SaleStatusEnum.CANCELED);
        update(sale);
    }

    @Override
    public Sale getById(Long id) throws SaleNotFoundException {
        Optional<Sale> sellOptional = saleRepository.getById(id);
        if (sellOptional.isAbsent())
            throw new SaleNotFoundException();
        return sellOptional.get();
    }

    @Override
    public List<Sale> findByCustomerId(Long id) {
        return saleRepository.findByCustomerId(id);
    }

    @Override
    public List<Sale> findByDate(Date date) {
        return null;
    }
}
