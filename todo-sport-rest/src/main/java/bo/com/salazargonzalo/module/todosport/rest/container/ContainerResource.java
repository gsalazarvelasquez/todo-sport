package bo.com.salazargonzalo.module.todosport.rest.container;

import bo.com.salazargonzalo.module.shared.rest.security.annotations.LoggedIn;
import bo.com.salazargonzalo.module.todosport.app.container.exception.ContainerNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.container.model.Container;
import bo.com.salazargonzalo.module.todosport.service.IContainerService;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("Containers")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Stateless
public class ContainerResource {

    @Inject
    private Logger log;

    @Inject
    public IContainerService containerService;

    @POST
    @LoggedIn
    public Response addContainer(Container container) {
        try {
            container = containerService.add(container);
            return Response
                    .ok()
                    .entity(container.getId())
                    .build();
        } catch (Exception e) {
            log.info(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @GET
    public Response getContainerList() {
        try {
            final List<Container> containerList = containerService.findAll();
            return Response
                    .ok()
                    .entity(containerList)
                    .build();

        } catch (RuntimeException e) {
            log.error("RuntimeException: " + e.getMessage());
            return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();

        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @PUT
    @LoggedIn
    public Response updateContainer(Container container) {
        try {
            containerService.update(container);
            return Response
                    .ok()
                    .build();
        } catch (ContainerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteContainer(@PathParam("id") Long id) {
        try {
            containerService.delete(id);
            return Response
                    .ok()
                    .build();
        } catch (ContainerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @LoggedIn
    @Path("/{id}")
    public Response getContainerById(@PathParam("id") Long id) {
        log.info("param: " + id);
        try {
            Container container = containerService.getById(id);
            return Response
                    .ok()
                    .entity(container)
                    .build();
        } catch (ContainerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}