package bo.com.salazargonzalo.module.todosport.app.shoe.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ShoeExistentException extends RuntimeException {
}
