package bo.com.salazargonzalo.module.todosport.app.container.service;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.todosport.app.container.exception.ContainerNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.container.model.Container;
import bo.com.salazargonzalo.module.todosport.app.container.repository.ContainerRepository;
import bo.com.salazargonzalo.module.todosport.service.IContainerService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class ContainerServiceImpl implements IContainerService {

    @Inject
    private ContainerRepository containerRepository;

    @Override
    public List<Container> findAll() {
        return containerRepository.findAllEnabled();
    }

    @Override
    public Container add(Container container) {
        container.setStatus(StatusEnum.ENABLED);
        return containerRepository.save(container);
    }

    @Override
    public void update(Container container) throws ContainerNotFoundException {
        containerRepository.update(container);
    }

    @Override
    public Container getById(Long id) throws ContainerNotFoundException {
        Optional<Container> containerOptional = containerRepository.getById(id);
        if (containerOptional.isAbsent())
            throw new ContainerNotFoundException();
        return containerOptional.get();
    }

    @Override
    public void delete(Long id) throws ContainerNotFoundException {
        Container container = getById(id);
        container.setStatus(StatusEnum.DELETED);
        containerRepository.save(container);

    }
}

