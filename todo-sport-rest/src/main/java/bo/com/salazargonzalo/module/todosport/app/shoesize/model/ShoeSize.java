package bo.com.salazargonzalo.module.todosport.app.shoesize.model;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "SHOE_SIZES")
@Getter
@Setter
@ToString
public class ShoeSize {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SHOE_SIZE_ID")
    private Long id;

    @Column(name = "SIZE_RANGE")
    private String sizeRange;

    @Column(name = "IS_VISIBLE")
    private Boolean isVisible;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    public ShoeSize() {
    }

    public ShoeSize(Long id) {
        this.id = id;
    }
}
