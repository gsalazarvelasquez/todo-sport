package bo.com.salazargonzalo.module.todosport.app.saledetail.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class SaleDetailNotFoundException extends RuntimeException {
}
