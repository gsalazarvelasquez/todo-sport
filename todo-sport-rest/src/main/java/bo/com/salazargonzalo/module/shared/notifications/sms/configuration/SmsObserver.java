package bo.com.salazargonzalo.module.shared.notifications.sms.configuration;

import bo.com.salazargonzalo.module.todosport.app.notification.model.Notification;
import bo.com.salazargonzalo.module.todosport.service.notifications.NotificationService;
import org.jboss.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;

@Stateless
public class SmsObserver implements Serializable {

    @Inject
    private Logger logger;
    @Inject
    private SmsSender smsSender;
    @Inject
    private NotificationService notificationService;


    @Asynchronous
    public void send(Notification notification) {
        notification = smsSender.sendSms(notification);
        notificationService.update(notification);
    }
}
