package bo.com.salazargonzalo.module.todosport.app.sale.repository;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.shared.utils.LongUtil;
import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;

import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class SaleRepository extends Repository {

    public List<Sale> findAll() {
        return findAll(Sale.class);
    }

    public List<Sale> findAllEnabled() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("status", StatusEnum.ENABLED);
        return findBy(Sale.class, filters);
    }

    public List<Sale> findByCustomerId(Long id) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("customer.id", id);
        return findBy(Sale.class, filters);
    }

    public Optional<Sale> getById(final Long id) {
        return LongUtil.isEmpty(id) ? Optional.absent() : getById(Sale.class, id);
    }

    public void deleteAll() {
        delete(findAll());
    }
}
