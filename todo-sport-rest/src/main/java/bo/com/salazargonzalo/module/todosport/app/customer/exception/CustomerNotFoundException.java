package bo.com.salazargonzalo.module.todosport.app.customer.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class CustomerNotFoundException extends RuntimeException {
}
