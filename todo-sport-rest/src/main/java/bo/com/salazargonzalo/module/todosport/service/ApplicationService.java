package bo.com.salazargonzalo.module.todosport.service;

import bo.com.salazargonzalo.module.todosport.app.customer.repository.CustomerRepository;
import bo.com.salazargonzalo.module.todosport.app.sale.repository.SaleRepository;
import bo.com.salazargonzalo.module.todosport.app.saledetail.repository.SaleDetailRepository;
import bo.com.salazargonzalo.module.todosport.app.shoe.repository.ShoeRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ApplicationService {

    @Inject
    private SaleDetailRepository saleDetailRepository;

    @Inject
    private SaleRepository saleRepository;

    @Inject
    private ShoeRepository shoeRepository;

    @Inject
    private CustomerRepository customerRepository;

    public void resetData() {
        saleDetailRepository.deleteAll();
        saleRepository.deleteAll();
        shoeRepository.deleteAll();
        customerRepository.deleteAll();
    }
}

