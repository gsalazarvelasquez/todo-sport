package bo.com.salazargonzalo.module.todosport.app.shoe.service;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;
import bo.com.salazargonzalo.module.todosport.app.saledetail.model.SaleDetail;
import bo.com.salazargonzalo.module.todosport.app.shoe.exception.ShoeNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoe.model.Shoe;
import bo.com.salazargonzalo.module.todosport.app.shoe.repository.ShoeRepository;
import bo.com.salazargonzalo.module.todosport.service.ISaleService;
import bo.com.salazargonzalo.module.todosport.service.ISalesDetailService;
import bo.com.salazargonzalo.module.todosport.service.IShoeService;
import bo.com.salazargonzalo.module.todosport.service.configuration.ConfigurationKey;
import bo.com.salazargonzalo.module.todosport.service.configuration.KeyAppConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Stateless
public class ShoeServiceImpl implements IShoeService {

    @Inject
    private ShoeRepository shoeRepository;

    @Inject
    private ISaleService saleService;

    @Inject
    private ISalesDetailService salesDetailService;

    @Inject
    private Logger log;

    @Override
    public Shoe add(Shoe shoe) {
        shoe.setStatus(StatusEnum.ENABLED);
        return shoeRepository.save(shoe);
    }

    @Override
    public List<Shoe> findAll() {
        return shoeRepository.findAllEnabled();
    }

    @Override
    public Shoe getById(Long id) throws ShoeNotFoundException {
        Optional<Shoe> shoeOptional = shoeRepository.getById(id);
        if (shoeOptional.isAbsent())
            throw new ShoeNotFoundException();
        return shoeOptional.get();
    }

    @Override
    public List<String> findContainersByCode(String code) throws ShoeNotFoundException {
        List<Shoe> shoeList = shoeRepository.findByCode(code);
        if (shoeList.isEmpty())
            throw new ShoeNotFoundException();
        return getContainerOrSizeList(shoeList, "container");
    }

    @Override
    public List<String> findSizesByCodeAndContainer(String code, String container) throws ShoeNotFoundException {
        List<Shoe> shoeList = shoeRepository.findByCodeAndContainer(code, container);
        if (shoeList.isEmpty())
            throw new ShoeNotFoundException();
        return getContainerOrSizeList(shoeList, "size");
    }

    public Shoe getByCodeContainerAndSize(String code, String container, String size)
            throws ShoeNotFoundException {
        Optional<Shoe> shoeOptional = shoeRepository.getByCodeContainerAndSize(code, container, size);
        if (shoeOptional.isAbsent())
            throw new ShoeNotFoundException();
        return shoeOptional.get();
    }

    private List<String> getContainerOrSizeList(List<Shoe> shoeList, String shoeParameterToFind) {
        List<String> list = new ArrayList<>();
        String param;
        for (Shoe shoe : shoeList) {
            boolean isDifferent = true;
            param = shoeParameterToFind.equals("size") ? shoe.getSize() : shoe.getContainer();
            for (String item : list)
                if (item.equals(param))
                    isDifferent = false;
            if (isDifferent)
                list.add(param);
        }
        return list;
    }

    @Override
    public void updateStockBySaleId(Long id) throws SaleNotFoundException {
        Sale sale = saleService.getById(id);
        List<SaleDetail> saleDetailList = salesDetailService.findBySaleId(id);
        switch (sale.getStatus()) {
            case PAID:
                this.reduceStock(saleDetailList);
                break;
            case CANCELED:
                this.restoreStock(saleDetailList);
                break;
            default:
                log.info("SaleDetail Empty");
                break;
        }
    }

    private void reduceStock(List<SaleDetail> saleDetails) {
        for (SaleDetail saleDetail : saleDetails) {
            Shoe shoe = getById(saleDetail.getShoe().getId());

            final int stockPar = shoe.getStockPar() - saleDetail.getPar();
            double stockDozen = Double.parseDouble(shoe.getStockDozen()) - Double.parseDouble(saleDetail.getDozen());

            shoe.setStockPar(stockPar);
            shoe.setStockDozen(Double.toString(stockDozen));

            shoeRepository.update(shoe);
        }
    }

    private void restoreStock(List<SaleDetail> saleDetails) {
        for (SaleDetail saleDetail : saleDetails) {
            Shoe shoe = saleDetail.getShoe();
            double stockDozen = Double.parseDouble(shoe.getStockDozen()) + Double.parseDouble(saleDetail.getDozen());
            shoe.setStockPar(shoe.getStockPar() + saleDetail.getPar());
            shoe.setStockDozen(Double.toString(stockDozen));
            shoeRepository.update(shoe);
        }
    }

    @Override
    public void delete(Long id) throws ShoeNotFoundException {
        Shoe shoe = getById(id);
        shoe.setStatus(StatusEnum.DELETED);
        shoeRepository.update(shoe);
    }

    @Override
    public void update(Shoe shoe) throws ShoeNotFoundException {
        shoeRepository.update(shoe);
    }

    @Override
    public void uploadShoePhoto(Long id, MultipartFormDataInput input) throws ShoeNotFoundException, IOException {
        final String UPLOADED_FILE_PATH = KeyAppConfiguration.getString(ConfigurationKey.SYSTEM_PATH_PHOTO_SHOES);

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        String fileType = uploadForm.get("fileType").get(0).getBodyAsString();

        Shoe shoe = getById(id);
        String fileName = getNewFile(shoe, fileType);
        shoe.setPhoto("images/" + fileName);

        List<InputPart> inputParts = uploadForm.get("image");

        for (InputPart inputPart : inputParts) {
            try {
                @SuppressWarnings("unused")
                MultivaluedMap<String, String> header = inputPart.getHeaders();

                InputStream inputStream = inputPart.getBody(InputStream.class, null);

                byte[] bytes = IOUtils.toByteArray(inputStream);
                // constructs upload file path
                fileName = UPLOADED_FILE_PATH + fileName;
                writeFile(bytes, fileName);
                log.info("Upload photo Success !!!!!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        update(shoe);
    }

    @Override
    public void importShoesExcelFile(MultipartFormDataInput input) throws IOException, InvalidFormatException {
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("excelFile");
        InputStream inputStream = inputParts.get(0).getBody(InputStream.class, null);

        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);

        sheet.forEach(row -> {
            Shoe shoe = new Shoe();
            row.forEach(cell -> parseCellToShoeAndStore(cell, shoe));
            if (!isShoeEmpty(shoe))
                add(shoe);
        });
        workbook.close();
    }

    private void parseCellToShoeAndStore(Cell cell, Shoe shoe) {
        if (cell.getRow().getRowNum() > 0) {
            switch (cell.getColumnIndex()) {
                case 0:
                    shoe.setCode(cell.getStringCellValue());
                    break;
                case 1:
                    shoe.setPhoto(cell.getStringCellValue());
                    break;
                case 2:
                    shoe.setContainer(cell.getStringCellValue());
                    break;
                case 3:
                    shoe.setSize(cell.getStringCellValue());
                    break;
                case 4:
                    shoe.setPar((int) cell.getNumericCellValue());
                    shoe.setStockPar(shoe.getPar());
                    break;
                case 5:
                    shoe.setDozen(String.valueOf(cell.getNumericCellValue()));
                    shoe.setStockDozen(shoe.getDozen());
                    break;
                case 6:
                    shoe.setPrice(String.valueOf(cell.getNumericCellValue()));
                    break;
                case 7:
                    shoe.setTotal(String.valueOf(cell.getNumericCellValue()));
                    break;
            }

        }
    }

    private void writeFile(byte[] content, String filename) throws IOException {
        File file = new File(filename);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fop = new FileOutputStream(file);
        fop.write(content);
        fop.flush();
        fop.close();
    }

    private String getNewFile(Shoe shoe, String fileType) {
        String[] imageType = fileType.split("/");
        return shoe.getCode() + '-' +
                shoe.getContainer() + "." +
                imageType[1];
    }

    private boolean isShoeEmpty(Shoe shoe) {
        return shoe.getCode() == null ||
                shoe.getPhoto() == null ||
                shoe.getContainer() == null ||
                shoe.getSize() == null ||
                shoe.getPar() == null ||
                shoe.getDozen() == null ||
                shoe.getPrice() == null ||
                shoe.getTotal() == null ||
                shoe.getStockPar() == null ||
                shoe.getStockDozen() == null;
    }
}

