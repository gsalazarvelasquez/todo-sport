package bo.com.salazargonzalo.module.todosport.app.saledetail.service;

import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;
import bo.com.salazargonzalo.module.todosport.app.saledetail.exception.SaleDetailNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.saledetail.model.SaleDetail;
import bo.com.salazargonzalo.module.todosport.app.saledetail.repository.SaleDetailRepository;
import bo.com.salazargonzalo.module.todosport.app.shoe.model.Shoe;
import bo.com.salazargonzalo.module.todosport.app.shoe.repository.ShoeRepository;
import bo.com.salazargonzalo.module.todosport.service.ISaleService;
import bo.com.salazargonzalo.module.todosport.service.ISalesDetailService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class SaleDetailServiceImpl implements ISalesDetailService {

    @Inject
    private SaleDetailRepository saleDetailRepository;

    @Inject
    private ShoeRepository shoeRepository;

    @Inject
    private ISaleService saleService;

    @Override
    public void add(List<SaleDetail> saleDetails) {
        saleDetailRepository.saveAndFlushAll(saleDetails);
    }

    @Override
    public void update(SaleDetail saleDetail) {

    }

    @Override
    public SaleDetail getById(Long id) throws SaleDetailNotFoundException {
        Optional<SaleDetail> saleDetail = saleDetailRepository.getById(id);
        if (saleDetail.isAbsent())
            throw new SaleDetailNotFoundException();
        return saleDetail.get();
    }

    @Override
    public List<SaleDetail> findBySaleId(Long saleId) throws SaleNotFoundException {
        return saleDetailRepository.findBySaleId(saleId);
    }

    @Override
    public void delete(Long id) throws SaleDetailNotFoundException {
        SaleDetail saleDetail = getById(id);
        saleDetailRepository.remove(saleDetail);
    }

    @Override
    public void deleteBySaleId(Long id) throws SaleNotFoundException {
        Sale sale = saleService.getById(id);
        List<SaleDetail> saleDetails = saleDetailRepository.findBySaleId(sale.getId());
        for (SaleDetail saleDetail : saleDetails) {
            Shoe shoe = saleDetail.getShoe();

            final int stockPar = shoe.getStockPar() + saleDetail.getPar();
            final double stockDozen = Double.parseDouble(shoe.getStockDozen()) + Double.parseDouble(saleDetail.getDozen());

            shoe.setStockPar(stockPar);
            shoe.setStockDozen(Double.toString(stockDozen));

            shoeRepository.update(shoe);
        }
        saleDetailRepository.delete(saleDetails);
    }
}
