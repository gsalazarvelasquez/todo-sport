package bo.com.salazargonzalo.module.shared.notifications.mail;

import bo.com.salazargonzalo.module.shared.notifications.mail.configuration.Mail;
import bo.com.salazargonzalo.module.shared.notifications.mail.configuration.MailSender;
import bo.com.salazargonzalo.module.todosport.app.notification.model.Notification;
import bo.com.salazargonzalo.module.todosport.service.notifications.NotificationService;
import org.jboss.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class MailObserver {

    @Inject
    private Logger logger;
    @Inject
    private NotificationService notificationService;

    @Asynchronous
    public void sendMail(Mail fields) {
        MailSender baseMailer = new MailSender(fields);
        baseMailer.sendEmail();
    }

    @Asynchronous
    public void send(Notification notification) {
        logger.info("notification: " + notification.getId());
        MailSender mailSender = new MailSender();
        notification = mailSender.sendEmail(notification);
        notificationService.update(notification);
    }
}
