package bo.com.salazargonzalo.module.todosport.app.notification.model;

import bo.com.salazargonzalo.domain.enumerator.NotificationTypeEnum;
import bo.com.salazargonzalo.domain.enumerator.PaymentStatusEnum;
import bo.com.salazargonzalo.module.shared.utils.EntityUtil;
import bo.com.salazargonzalo.module.shared.utils.LongUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notifications")
@Getter
@Setter
@ToString
public class Notification {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "from_name")
    private String fromName;

    @Column(name = "from_address")
    private String fromAddress;

    @Column(name = "to_address")
    private String toAddress;

    @Column(name = "subject")
    private String subject;

    @Column(name = "content")
    private String content;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PaymentStatusEnum status;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private NotificationTypeEnum type;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "sent_date")
    private Date sentDate;

    @Column(name = "error_description")
    private String errorDescription;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "device_id")
    private Long deviceId;


    @Transient
    public boolean isNew() {
        return LongUtil.isEmpty(id);
    }

    public static Notification createNew() {
        Notification notification = new Notification();
        notification.fromName = EntityUtil.DEFAULT_STRING;
        notification.fromAddress = EntityUtil.DEFAULT_STRING;
        notification.toAddress = EntityUtil.DEFAULT_STRING;
        notification.subject = EntityUtil.DEFAULT_STRING;
        notification.content = EntityUtil.DEFAULT_STRING;
        notification.status = PaymentStatusEnum.PENDING;
        notification.createdDate = EntityUtil.DEFAULT_DATE;
        notification.sentDate = EntityUtil.DEFAULT_DATE;
        notification.errorDescription = EntityUtil.DEFAULT_STRING;
        return notification;
    }
}
