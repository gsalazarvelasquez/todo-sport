package bo.com.salazargonzalo.module.todosport.service;


import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;

import java.util.Date;
import java.util.List;

public interface ISaleService {

    Sale add(Sale sale);

    void update(Sale sale) throws SaleNotFoundException;

    void remove(Long id) throws SaleNotFoundException;

    Sale getById(Long id) throws SaleNotFoundException;

    List<Sale> findByCustomerId(Long id);

    List<Sale> findByDate(Date date);

    List<Sale> findAll();
}
