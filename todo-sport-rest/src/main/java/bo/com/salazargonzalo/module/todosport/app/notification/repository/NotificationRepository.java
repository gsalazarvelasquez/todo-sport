package bo.com.salazargonzalo.module.todosport.app.notification.repository;

import bo.com.salazargonzalo.domain.enumerator.PaymentStatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.todosport.app.notification.model.Notification;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class NotificationRepository extends Repository {

    public Optional<Notification> findById(Long id) {
        return getById(Notification.class, id);
    }

    public List<Notification> findWithPendingStatus() {
        Query query = entityManager.createQuery("SELECT p "
                + "FROM Notification p "
                + "WHERE p.status = :status ");
        query.setParameter("status", PaymentStatusEnum.PENDING);
        return query.getResultList();
    }
}
