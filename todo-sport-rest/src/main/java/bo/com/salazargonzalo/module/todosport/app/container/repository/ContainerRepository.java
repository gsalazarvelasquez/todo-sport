package bo.com.salazargonzalo.module.todosport.app.container.repository;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.shared.utils.LongUtil;
import bo.com.salazargonzalo.module.todosport.app.container.model.Container;

import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class ContainerRepository extends Repository {

    public List<Container> findAll() {
        return findAll(Container.class);
    }

    public List<Container> findAllEnabled() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("status", StatusEnum.ENABLED);
        return findBy(Container.class, filters);
    }

    public Optional<Container> getById(final Long id) {
        return LongUtil.isEmpty(id) ? Optional.absent() : getById(Container.class, id);
    }
}
