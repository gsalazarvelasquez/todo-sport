package bo.com.salazargonzalo.module.shared.gcm.observer;

import bo.com.salazargonzalo.module.shared.gcm.NotificationSender;
import bo.com.salazargonzalo.module.todosport.app.notification.model.Notification;
import org.jboss.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class PushNotificationObserver {

    @Inject
    private Logger logger;
    @Inject
    private NotificationSender notificationSender;


    @Asynchronous
    public void send(Notification notification) {
        notificationSender.sendNotification(notification);
    }
}
