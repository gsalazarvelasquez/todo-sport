package bo.com.salazargonzalo.module.todosport.app.container.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ContainerNotFoundException extends RuntimeException {
}
