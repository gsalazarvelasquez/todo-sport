package bo.com.salazargonzalo.module.todosport.app.account.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class AccountNotFoundException extends RuntimeException {
}
