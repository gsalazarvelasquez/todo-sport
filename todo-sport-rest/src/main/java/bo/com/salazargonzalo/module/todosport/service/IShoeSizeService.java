package bo.com.salazargonzalo.module.todosport.service;


import bo.com.salazargonzalo.module.todosport.app.shoesize.exception.ShoeSizeNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoesize.model.ShoeSize;

import java.util.List;

public interface IShoeSizeService {

    ShoeSize add(ShoeSize shoeSize);

    void update(ShoeSize shoeSize) throws ShoeSizeNotFoundException;

    ShoeSize getById(Long id) throws ShoeSizeNotFoundException;

    void delete(Long id) throws ShoeSizeNotFoundException;

    List<ShoeSize> findAll();
}
