package bo.com.salazargonzalo.module.shared.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class RoleNotFoundException extends RuntimeException {
}
