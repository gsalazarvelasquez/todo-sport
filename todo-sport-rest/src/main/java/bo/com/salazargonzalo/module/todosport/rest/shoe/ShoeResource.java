package bo.com.salazargonzalo.module.todosport.rest.shoe;

import bo.com.salazargonzalo.module.shared.rest.security.annotations.LoggedIn;
import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoe.exception.ShoeNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoe.model.Shoe;
import bo.com.salazargonzalo.module.todosport.service.IShoeService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.util.List;

@Path("Shoes")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Stateless
public class ShoeResource {

    @Inject
    private Logger log;

    @Inject
    public IShoeService shoeService;

    @PUT
    @LoggedIn
    public Response updateStockBySaleId(Shoe shoe) {
        try {
            shoeService.update(shoe);
            return Response
                    .ok()
                    .build();
        } catch (ShoeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }


    @PUT
    @Path("{id}/uploadPhoto")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response updateShoePhoto(@PathParam("id") Long id, MultipartFormDataInput input) {

        try {
            shoeService.uploadShoePhoto(id, input);
            return Response
                    .ok()
                    .build();

        } catch (ShoeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();

        } catch (IOException e) {
            log.error("IO Exception: " + e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("importExcel")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response importExcelShoe(MultipartFormDataInput input) {

        try {
            shoeService.importShoesExcelFile(input);
            return Response
                    .ok()
                    .build();

        } catch (ShoeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();

        } catch (IOException e) {
            log.error("IO Exception: " + e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();

        } catch (InvalidFormatException e) {
            log.error("InvalidFormatException: " + e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @GET
    public Response getAllShoes() {
        try {
            final List<Shoe> shoeList = shoeService.findAll();
            return Response
                    .ok()
                    .entity(shoeList)
                    .build();

        } catch (RuntimeException e) {
            log.error("RuntimeException: " + e.getMessage());
            return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();

        } catch (Exception e) {
            log.error("[ERROR] Exception: " + e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @PUT
    @LoggedIn
    @Path("updateStock/query/")
    public Response updateStockBySaleId(@QueryParam("saleId") Long saleId) {
        try {
            shoeService.updateStockBySaleId(saleId);
            return Response
                    .ok()
                    .build();
        } catch (SaleNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("query")
    public Response getShoesByCodeContainerAndSize(@QueryParam("code") String code,
                                                   @QueryParam("container") String container,
                                                   @QueryParam("size") String size) {
        try {
            Shoe shoe = shoeService.getByCodeContainerAndSize(code, container, size);
            return Response
                    .ok()
                    .entity(shoe)
                    .build();

        } catch (ShoeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("containers/query")
    public Response findShoeContainersByCode(@QueryParam("code") String code) {
        try {
            List<String> containers = shoeService.findContainersByCode(code);
            return Response
                    .ok()
                    .entity(containers)
                    .build();
        } catch (ShoeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("sizes/query")
    public Response findShoeSizeByCodeAndContainer(@QueryParam("code") String code,
                                                   @QueryParam("container") String container) {
        try {
            List<String> sizes = shoeService.findSizesByCodeAndContainer(code, container);
            return Response
                    .ok()
                    .entity(sizes)
                    .build();

        } catch (ShoeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteShoe(@PathParam("id") Long id) {
        try {
            shoeService.delete(id);
            return Response
                    .ok()
                    .build();
        } catch (ShoeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}