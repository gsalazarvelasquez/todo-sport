package bo.com.salazargonzalo.module.todosport.rest.customer;

import bo.com.salazargonzalo.module.shared.rest.security.annotations.LoggedIn;
import bo.com.salazargonzalo.module.todosport.app.customer.exception.CustomerNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.customer.model.Customer;
import bo.com.salazargonzalo.module.todosport.service.ICustomerService;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("Customers")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Stateless
public class CustomerResource {

    @Inject
    private Logger log;

    @Inject
    public ICustomerService customerService;

    @POST
    @LoggedIn
    public Response addCustomer(Customer customer) {
        try {
            customer = customerService.add(customer);
            return Response
                    .ok()
                    .entity(customer.getId())
                    .build();

        } catch (Exception e) {
            log.info("[ERROR]:" + e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @PUT
    @LoggedIn
    @Path("/{id}")
    public Response updateCustomer(@PathParam("id") Long id, Customer customer) {
        try {
            customerService.findById(id);
            customerService.update(customer);
            return Response
                    .ok()
                    .build();
        } catch (CustomerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @LoggedIn
    @Path("/{id}")
    public Response getCustomerById(@PathParam("id") Long id) {
        try {
            Customer customer = customerService.findById(id);
            return Response
                    .ok()
                    .entity(customer)
                    .build();
        } catch (CustomerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @LoggedIn
    public Response getAllCustomers() {
        try {
            final List<Customer> customers = customerService.getAll();
            return Response
                    .ok()
                    .entity(customers)
                    .build();
        } catch (Exception e) {
            log.error("[ERROR] Exception: " + e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteCustomer(@PathParam("id") Long id) {
        try {
            customerService.delete(id);
            return Response
                    .ok()
                    .build();
        } catch (CustomerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}