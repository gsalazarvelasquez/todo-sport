package bo.com.salazargonzalo.module.todosport.app.account.service;

import bo.com.salazargonzalo.domain.dao.VerificationTokenDao;
import bo.com.salazargonzalo.domain.dto.AccountDto;
import bo.com.salazargonzalo.domain.dto.UserManagementDto;
import bo.com.salazargonzalo.domain.entities.VerificationTokenEntity;
import bo.com.salazargonzalo.domain.enumerator.SellerEnum;
import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.exception.InvalidInputException;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.rest.security.Identity;
import bo.com.salazargonzalo.module.shared.utils.AppConfiguration;
import bo.com.salazargonzalo.module.todosport.app.account.model.Account;
import bo.com.salazargonzalo.module.todosport.app.account.repository.AccountRepository;
import bo.com.salazargonzalo.module.todosport.app.configuration.repository.ConfigurationRepository;
import bo.com.salazargonzalo.module.todosport.app.user.model.User;
import bo.com.salazargonzalo.module.todosport.app.user.repository.UserRepository;
import bo.com.salazargonzalo.module.todosport.app.user.service.UserService;
import bo.com.salazargonzalo.module.todosport.app.userrole.model.UserRole;
import bo.com.salazargonzalo.module.todosport.app.userrole.service.UserRoleService;
import bo.com.salazargonzalo.module.todosport.service.configuration.ConfigurationKey;
import bo.com.salazargonzalo.module.todosport.service.configuration.KeyAppConfiguration;
import org.jboss.logging.Logger;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class AccountService {

    @Inject
    private Logger log;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    @Inject
    private AccountRepository accountDao;

    @Inject
    private ConfigurationRepository configurationRepository;

    @Inject
    private VerificationTokenDao verificationTokenDao;

    @Inject
    private UserRoleService userRoleService;

    @Inject
    private AppConfiguration labels;


    public List<AccountDto> findAccountsByUserLogged(Identity identity) {


        if (identity.getIdentityDto().isBuhoAdmin()) {
            return findAllAccounts();
        }

        if (identity.getIdentityDto().isUbidataAdmin()) {
            return filterBySeller();
        }
        return Collections.emptyList();
    }

    private List<AccountDto> filterBySeller() {
        List<UserRole> userRoles = userRoleService.findByRoleId(2L);
        List<User> usersFiltered = userRoles
                .stream()
                .filter(userRole -> userRole
                        .getUser()
                        .getAccount()
                        .getSeller() == SellerEnum.UBIDATA)
                .map(UserRole::getUser)
                .collect(Collectors.toList());
        return AccountDto.convertFromUserList(usersFiltered);
    }

    private List<AccountDto> findAllAccounts() {
        List<UserRole> userList = userRoleService.findByRoleId(2L);
        List<User> userEntityList = userList.stream().map(UserRole::getUser).collect(Collectors.toList());
        return AccountDto.convertFromUserList(userEntityList);
    }

    public Account createFrom(AccountDto accountDto, Identity identity) {
        User user = AccountDto.from(accountDto);
        Account account = accountDto.fromDto();
        return storeAccount(account, user, identity);
    }

    private void validateAccount(Account detachedAccount) throws InvalidInputException {
        Optional<Account> accountOptional = accountDao.getByName(detachedAccount.getName());
        if (accountOptional.isPresent()) {
            throw new InvalidInputException(labels.getMessage("msg.account.name.registered"));
        }
    }

    public Account store(Account detachedAccount) {
        return accountDao.merge(detachedAccount);
    }

    public Account storeAccount(Account detachedAccount, User detachedUser, Identity identity) {
        Account account;

        if (detachedAccount.isNew()) {
            validateAccount(detachedAccount);
            detachedAccount.setSeller(identity.getIdentityDto().isBuhoAdmin() ? SellerEnum.SWISSBYTES : identity.getIdentityDto().isUbidataAdmin() ? SellerEnum.UBIDATA : SellerEnum.SWISSBYTES);
            account = store(detachedAccount);
            log.info(String.format("Account %s is registered!", account.getName()));

            if (detachedUser.isNew()) {
                detachedUser.setAccount(account);
                userService.validateUser(detachedUser);
                User user = userService.storeUser(detachedUser);
                setRolesToAdminAccount(user);
            }
        } else {
            account = store(detachedAccount);
            userService.storeUser(detachedUser);
        }
        return account;
    }

    public Account update(Account detachedAccount) throws EJBException {
        if (detachedAccount.getNit().length() > 20)
            throw new EJBException(labels.getMessage("msg.invalid.nit"));

        if (detachedAccount.getDocumentNumber().length() > 10)
            throw new EJBException(labels.getMessage("msg.invalid.document.number"));

        if (detachedAccount.getSocialReason().length() > 40)
            throw new EJBException(labels.getMessage("msg.invalid.social.reason"));

        return accountDao.merge(detachedAccount);
    }

    private User storeAccount(Account detachedAccount, User detachedUser) throws EJBException {
        log.info(String.format("account=> %s user=> %s", detachedAccount, detachedUser));
        validateAccount(detachedAccount);
        userService.validateUser(detachedUser);
        return store(detachedAccount, detachedUser);
    }

    private User store(Account detachedAccount, User detachedUser) {
        Account account;
        User user = User.createNew();
        if (detachedAccount.isNew()) {
            account = accountDao.merge(detachedAccount);

            if (detachedAccount.isNew()) {
                detachedUser.setAccount(account);
                user = userService.storeUser(detachedUser);
                setRolesToAdminAccount(user);
            }
        } else {
            accountDao.merge(detachedAccount);
            user = userService.storeUser(detachedUser);
        }
        return user;
    }

    public User storeAccount(UserManagementDto userManagementDto) {
        Account account = Account.createNew();
        account.setName(userManagementDto.getName());
        account.setEmail(userManagementDto.getEmail());
        User user = userManagementDto.toUserEntity();
        return storeAccount(account, user);
    }

    public User storeAccountFromWeb(UserManagementDto userManagementDto) {
        Account account = Account.createNew();
        account.setName(userManagementDto.getName());
        account.setEmail(userManagementDto.getEmail());
        User user = userManagementDto.toUserEntity();
        return store(account, user);
    }


    public void delete(Account account) {
        Long accountId = account.getId();

        List<VerificationTokenEntity> tokenEntityList = verificationTokenDao.findByCompanyId(accountId);
        List<User> userList = userRepository.findByAccountId(accountId);
        accountDao.delete(tokenEntityList);
        userService.delete(userList);

        account.setStatus(StatusEnum.DISABLED);
        accountDao.merge(account);
    }

    private void setRolesToAdminAccount(User user) {
        String rolesAdmin = KeyAppConfiguration.getString(ConfigurationKey.ROLE_ADMIN_ID);
        for (String roleId : rolesAdmin.split(";")) {
            UserRole userRole = new UserRole(user.getId(), Long.parseLong(roleId));
            userRoleService.storeUserRole(userRole);
        }
    }

    public boolean isTestAccount(Long accountId) {
        log.info("isTestAccount: " + accountId + " == " + configurationRepository.getCompanyTestId());
        return accountId == configurationRepository.getCompanyTestId();
    }
}
