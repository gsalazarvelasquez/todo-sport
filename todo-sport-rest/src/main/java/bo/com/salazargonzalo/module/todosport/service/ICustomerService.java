package bo.com.salazargonzalo.module.todosport.service;

import bo.com.salazargonzalo.module.todosport.app.customer.exception.CustomerNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.customer.model.Customer;

import java.util.List;

public interface ICustomerService {

    Customer add(Customer customer);

    void update(Customer customer) throws CustomerNotFoundException;

    void delete(Long id) throws CustomerNotFoundException;

    Customer findById(Long id) throws CustomerNotFoundException;

    List<Customer> getAll();

}
