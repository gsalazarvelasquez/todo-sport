package bo.com.salazargonzalo.module.todosport.service;


import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.saledetail.exception.SaleDetailNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.saledetail.model.SaleDetail;

import java.util.List;

public interface ISalesDetailService {

    void add(List<SaleDetail> saleDetails);

    void update(SaleDetail saleDetail);

    SaleDetail getById(Long id) throws SaleDetailNotFoundException;

    List<SaleDetail> findBySaleId(Long saleId) throws SaleNotFoundException;

    void delete(Long id) throws SaleDetailNotFoundException;

    void deleteBySaleId(Long id) throws SaleDetailNotFoundException;
}
