package bo.com.salazargonzalo.module.todosport.app.saledetail.repository;

import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.shared.utils.OrderBy;
import bo.com.salazargonzalo.module.todosport.app.saledetail.model.SaleDetail;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class SaleDetailRepository extends Repository {

    public List<SaleDetail> findAll() {
        return findAll(SaleDetail.class);
    }

    public List<SaleDetail> findBySaleId(Long id) {
        Map<String, Object> filters = new HashMap<>();
        List<OrderBy> orderByList = new ArrayList<>();

        OrderBy orderBy = new OrderBy();
        orderBy.setSortField("shoe.code");
        orderBy.setSortOrder(OrderBy.ASCENDING);

        orderByList.add(orderBy);
        filters.put("sale.id", id);
        return findBy(SaleDetail.class, filters, orderByList);
    }

    public Optional<SaleDetail> getById(final Long id) {
        return getById(SaleDetail.class, id);
    }

    public void deleteAll() {
        delete(findAll());
    }
}
