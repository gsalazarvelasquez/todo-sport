package bo.com.salazargonzalo.module.todosport.rest.salesdetail;

import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;
import bo.com.salazargonzalo.module.todosport.app.saledetail.model.SaleDetail;
import bo.com.salazargonzalo.module.todosport.app.shoe.model.Shoe;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
class SaleDetailDto implements Serializable {
    private Long saleId;
    private Long shoeId;
    private Integer par;
    private String dozen;
    private String price;
    private String shoeSize;


    static List<SaleDetail> toSaleDetailList(List<SaleDetailDto> saleDetailDtoList) {
        List<SaleDetail> saleDetailList = new ArrayList<>();
        for (SaleDetailDto saleDetailDto : saleDetailDtoList) {
            SaleDetail saleDetail = new SaleDetail();
            saleDetail.setSale(new Sale(saleDetailDto.saleId));
            saleDetail.setShoe(new Shoe(saleDetailDto.shoeId));
            saleDetail.setPar(saleDetailDto.par);
            saleDetail.setDozen(saleDetailDto.dozen);
            saleDetail.setPrice(saleDetailDto.price);
            saleDetail.setShoeSize(saleDetailDto.shoeSize);
            // add to list
            saleDetailList.add(saleDetail);
        }
        return saleDetailList;
    }
}
