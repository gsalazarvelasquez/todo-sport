package bo.com.salazargonzalo.module.todosport.app.shoe.repository;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.shared.utils.LongUtil;
import bo.com.salazargonzalo.module.todosport.app.shoe.model.Shoe;

import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class ShoeRepository extends Repository {

    public List<Shoe> findAll() {
        return findAll(Shoe.class);
    }

    public List<Shoe> findAllEnabled() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("status", StatusEnum.ENABLED);
        return findBy(Shoe.class, filters);
    }

    public List<Shoe> findByCode(String code) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("code", code);
        filters.put("status", StatusEnum.ENABLED);
        return findBy(Shoe.class, filters);
    }

    public List<Shoe> findByCodeAndContainer(String code, String container) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("code", code);
        filters.put("container", container);
        filters.put("status", StatusEnum.ENABLED);
        return findBy(Shoe.class, filters);
    }

    public Optional<Shoe> getById(final Long id) {
        return LongUtil.isEmpty(id) ? Optional.absent() : getById(Shoe.class, id);
    }

    public Optional<Shoe> getByCodeContainerAndSize(String code, String container, String size) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("code", code);
        filters.put("container", container);
        filters.put("size", size);
        filters.put("status", StatusEnum.ENABLED);
        return getBy(Shoe.class, filters);
    }

    public void deleteAll() {
        delete(findAll());
    }
}
