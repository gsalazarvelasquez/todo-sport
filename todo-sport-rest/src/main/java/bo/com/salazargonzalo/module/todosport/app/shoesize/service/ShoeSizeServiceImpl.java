package bo.com.salazargonzalo.module.todosport.app.shoesize.service;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.todosport.app.shoesize.exception.ShoeSizeNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoesize.model.ShoeSize;
import bo.com.salazargonzalo.module.todosport.app.shoesize.repository.ShoeSizeRepository;
import bo.com.salazargonzalo.module.todosport.service.IShoeSizeService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class ShoeSizeServiceImpl implements IShoeSizeService {

    @Inject
    private ShoeSizeRepository shoeRepository;

    @Override
    public ShoeSize add(ShoeSize shoeSize) {
        shoeSize.setStatus(StatusEnum.ENABLED);
        return shoeRepository.save(shoeSize);
    }

    @Override
    public void update(ShoeSize shoeSize) throws ShoeSizeNotFoundException {
        shoeRepository.update(shoeSize);
    }

    @Override
    public ShoeSize getById(Long id) throws ShoeSizeNotFoundException {
        Optional<ShoeSize> shoeSizeOptional = shoeRepository.getById(id);
        if (shoeSizeOptional.isAbsent())
            throw new ShoeSizeNotFoundException();
        return shoeSizeOptional.get();
    }

    @Override
    public void delete(Long id) throws ShoeSizeNotFoundException {
        ShoeSize shoeSize = getById(id);
        shoeSize.setStatus(StatusEnum.DELETED);
        update(shoeSize);
    }

    @Override
    public List<ShoeSize> findAll() {
        return shoeRepository.findAllEnabled();
    }
}

