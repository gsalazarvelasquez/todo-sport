package bo.com.salazargonzalo.module.todosport.app.container.model;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "CONTAINERS")
@Getter
@Setter
@ToString
public class Container {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CONTAINER_ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "IS_VISIBLE")
    private Boolean isVisible;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    public Container() {
    }

    public Container(Long id) {
        this.id = id;
    }
}
