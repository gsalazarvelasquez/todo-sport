package bo.com.salazargonzalo.module.todosport.app.user.service;

import bo.com.salazargonzalo.domain.dao.VerificationTokenDao;
import bo.com.salazargonzalo.domain.dto.ChangePasswordDto;
import bo.com.salazargonzalo.domain.dto.UserManagementDto;
import bo.com.salazargonzalo.domain.entities.VerificationTokenEntity;
import bo.com.salazargonzalo.module.shared.exception.EmailAlreadyExistException;
import bo.com.salazargonzalo.module.shared.exception.FieldNotValidException;
import bo.com.salazargonzalo.module.shared.exception.InvalidInputException;
import bo.com.salazargonzalo.module.shared.exception.UsernameAlreadyExistException;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.rest.security.Identity;
import bo.com.salazargonzalo.module.shared.utils.AppConfiguration;
import bo.com.salazargonzalo.module.shared.utils.Encode;
import bo.com.salazargonzalo.module.shared.utils.LongUtil;
import bo.com.salazargonzalo.module.shared.utils.StringUtil;
import bo.com.salazargonzalo.module.todosport.app.account.model.Account;
import bo.com.salazargonzalo.module.todosport.app.account.service.AccountService;
import bo.com.salazargonzalo.module.todosport.app.role.model.Role;
import bo.com.salazargonzalo.module.todosport.app.user.model.User;
import bo.com.salazargonzalo.module.todosport.app.user.repository.UserRepository;
import bo.com.salazargonzalo.module.todosport.app.userrole.model.UserRole;
import bo.com.salazargonzalo.module.todosport.app.userrole.respository.UserRoleRepository;
import bo.com.salazargonzalo.module.todosport.app.userrole.service.UserRoleService;
import org.jboss.logging.Logger;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Stateless
public class UserService {

    private final long roleSystemAdmin = 1;
    private final long roleAccountAdmin = 2;
    private final long roleAdmin = 3;
    private final long roleTracker = 5;

    @Inject
    private Logger log;

    @Inject
    private AppConfiguration labels;

    @Inject
    private UserRoleRepository userRoleRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private VerificationTokenDao verificationTokenDao;

    @Inject
    private AccountService accountService;

    @Inject
    private Identity identity;

    @Inject
    private UserRoleService userRoleService;


    public User add(User user) {
        return userRepository.merge(user);
    }

    public void validateUser(User detachedUser) throws InvalidInputException {
        log.info("validateUser " + detachedUser);
        if (!detachedUser.isNew() && isAccountAdmin(detachedUser.getId()) && !isSystemAdmin()) {
            Optional<User> userEntityOptional = userRepository.getById(detachedUser.getId());

            if (userEntityOptional.isAbsent()) throw new InvalidInputException("Usuario no existe");

            if (!isChangingUserName(detachedUser, userEntityOptional.get()))
                throw new InvalidInputException("No tiene permisos para cambiar el nombre de usuario al dueno de la cuenta.");

            if (!isChangingUserRole(detachedUser, userEntityOptional.get()))
                throw new InvalidInputException("No tiene permisos para cambiar el rol al dueno de la cuenta.");
        }

        Optional<User> userEntityOptional = userRepository.getByUsername(detachedUser.getUsername());

        if (userEntityOptional.isPresent() && !userEntityOptional.get().getId().equals(detachedUser.getId())) {
            if (LongUtil.isNumeric(userEntityOptional.get().getUsername())) {
                throw new InvalidInputException("El número de telefono ya se encuentra registrado");
            }
            throw new InvalidInputException("El nombre de usuario ya se encuentra registrado");
        }

        userEntityOptional = userRepository.getByEmail(detachedUser.getEmail());
        if (userEntityOptional.isPresent() && userEntityOptional.get().getId() != detachedUser.getId()) {
            throw new InvalidInputException("El correo electronico ya se encuentra registrado");
        }
    }

    public void update(User user) throws UsernameAlreadyExistException, EmailAlreadyExistException, FieldNotValidException {
        validateEmail(user);
        validateUserName(user);
        add(user);
    }

    private void validateUserName(User userEntity) {
        final Optional<User> user = userRepository.findByUserName(userEntity.getUsername());
        if (user.isPresent() && !Objects.equals(userEntity.getId(), user.get().getId()))
            throw new UsernameAlreadyExistException();
    }

    private void validateEmail(User userEntity) {
        Optional<User> user = userRepository.getByEmail(userEntity.getEmail());
        if (user.isPresent() && !Objects.equals(user.get().getId(), userEntity.getId()))
            throw new EmailAlreadyExistException();
    }

    public void updateFrom(UserManagementDto[] userManagementDtoList) throws UsernameAlreadyExistException, EmailAlreadyExistException, FieldNotValidException {
        List<User> userList = new ArrayList<>();

        for (UserManagementDto userManagementDto : userManagementDtoList) {
            final Long userId = userManagementDto.getId();
            User user = userManagementDto.toUserEntity();
            user.setAccount(new Account(identity.getIdentityDto().getAccountId()));
            userList.add(user);
            userRoleService.updateRolUser(new UserRole(userId, userManagementDto.getRoleId()));
        }
        storeUser(userList);
    }

    private User getUserEntity(User user) {
        validateUser(user);

        if (user.getChangePassword()) {
            user.setGeneratedPassword(user.getPassword());
        } else {
            user.setGeneratedPassword("");
        }

        if (user.isNew()) {
            String passwordEncoded = Encode.encode(user.getPassword());
            user.setPassword(passwordEncoded);
        }
        return user;

    }

    public User storeUser(User detachedUser) throws RuntimeException {
        try {
            if (detachedUser.isNew()) {
                validateUser(detachedUser);
                String passwordEncoded = Encode.encode(detachedUser.getPassword());
                detachedUser.setPassword(passwordEncoded);
            } else {
                Optional<User> userEntity = userRepository.getById(detachedUser.getId());
                detachedUser.setPassword(userEntity.get().getPassword());
            }

            detachedUser = add(detachedUser);

            return detachedUser;

        } catch (EJBException ejbEx) {
            log.error(ejbEx.getMessage(), ejbEx);
            throw new InvalidInputException(ejbEx);
        }
    }

    private String storeUser(List<User> detachedUserList) {
        log.info(String.format("StoreUser | userList %s", detachedUserList.size()));
        String errorResult = "";
        for (User detachedUser : detachedUserList) {
            try {
                Optional<User> userOptional = userRepository.getById(detachedUser.getId());
                if (userOptional.isAbsent())
                    throw new InvalidInputException("El usuario no existe");
                detachedUser = storeUser(detachedUser);

            } catch (EJBException ejbEx) {
                InvalidInputException invalidInputException = new InvalidInputException(ejbEx);
                log.error(ejbEx.getMessage(), ejbEx);
                errorResult = errorResult + detachedUser.getName() + ": " + invalidInputException.getMessage();
                errorResult = errorResult + "\n";
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                errorResult = errorResult + detachedUser.getName() + ": " + e.getMessage();
                errorResult = errorResult + "\n";
            }
        }
        return errorResult;
    }

    public User changePassword(User detachedUser) throws EJBException {
        log.info(String.format("ChangePassword | user: %s ", detachedUser.getName()));
        if (detachedUser.isNew())
            throw new InvalidInputException("No existe el usuario con el id: " + detachedUser.getId());

        if (!isSystemAdmin() && detachedUser.getAccount() != null
                && accountService.isTestAccount(detachedUser.getAccount().getId()))
            throw new InvalidInputException("No se puede modificar la contrasena de la cuenta de Test");

        // parameters to change password
        detachedUser.setChangePassword(true);
        detachedUser.setGeneratedPassword(detachedUser.getPassword());
        return storePassword(detachedUser.getPassword(), detachedUser);
    }

    public User resetPasswordBySysAdmin(String password, User detachedUser) {
        return storePassword(password, detachedUser);
    }

    private User storePassword(String password, User user) {
        String passwordEncode = Encode.encode(password);
        user.setPassword(passwordEncode);
        return add(user);
    }

    private User changePasswordFirstLogin(User detachedUser) throws EJBException {
        log.info(String.format("ChangePassword | user: %s ", detachedUser.getName()));
        if (detachedUser.isNew())
            throw new InvalidInputException("No existe el usuario con el id: " + detachedUser.getId());

        detachedUser.setChangePassword(false);
        detachedUser.setGeneratedPassword("");

        String passwordEncoded = Encode.encode(detachedUser.getPassword());
        log.info("password : " + detachedUser.getPassword());
        detachedUser.setPassword(passwordEncoded);

        User user = add(detachedUser);
        log.info("UserEntity password changed: " + user.getUsername());
        return user;
    }


    public User changePassword(ChangePasswordDto dto) throws EJBException {
        log.info("changePassword => " + dto);

        if (!dto.getNewPassword().equals(dto.getConfirmPassword()))
            throw new InvalidInputException(labels.getMessage("passwords.are.not.equals"));

        Optional<User> userOptional = userRepository.getById(dto.getUserId());

        if (userOptional.isAbsent())
            throw new InvalidInputException(labels.getMessage("user.not.exist"));

        User user = userOptional.get();
        user.setPassword(dto.getNewPassword());
        return changePasswordFirstLogin(user);
    }

    public User changePasswordWithToken(User detachedUser, VerificationTokenEntity tokenEntity) throws EJBException {
        log.info(String.format("ChangePassword | user: %s verificationToken: %s ", detachedUser.getName(), tokenEntity.getId()));
        if (detachedUser.isNew()) {
            throw new InvalidInputException("No existe el usuario con el id: " + detachedUser.getId());
        }

        String passwordEncoded = Encode.encode(detachedUser.getPassword());
        detachedUser.setPassword(passwordEncoded);

        User user = add(detachedUser);

        tokenEntity.setVerified(true);
        userRepository.merge(tokenEntity);

        log.info("UserEntity password changed: " + user.getUsername());
        return user;
    }

    public void delete(List<User> userList) {
        userList.forEach(this::delete);
    }

    public void delete(User user) {
        Long userId = user.getId();

        List<UserRole> userRoleList = userRoleRepository.findByUserId(userId);
        if (!userRoleList.isEmpty()) userRoleRepository.delete(userRoleList);

        List<VerificationTokenEntity> verificationTokenList = verificationTokenDao.findByUserId(userId);
        if (!verificationTokenList.isEmpty()) verificationTokenDao.delete(verificationTokenList);

        userRepository.remove(user);
        log.info("user: " + userId + " removed!");
    }

    private void removeVerificationToken(Long userId) {
        log.info(String.format("removeVerificationToken | userId: %s ", userId));
        List<VerificationTokenEntity> verificationTokenList = verificationTokenDao.findByUserId(userId);

        for (VerificationTokenEntity verificationTokenEntity : verificationTokenList) {
            verificationTokenDao.remove(verificationTokenEntity);
        }
        log.info("Verification token  removed by id: " + userId);
    }

    public User validateEmail(String mail) throws RuntimeException {
        log.info(String.format("ValidateEmail | mail: %s", mail));

        if (StringUtil.isEmpty(mail)) {
            log.warn("email is empty!");
            throw new InvalidInputException("El correo electrónico es requerido");
        }

        Optional<User> entityOptional = userRepository.getByEmail(mail);
        if (entityOptional.isAbsent()) {
            log.warn("Does not exist email!");
            throw new InvalidInputException("No se pudo encontrar el correo electrónico proporcionado");
        }

        return entityOptional.get();
    }

    public List<User> findByUserId(Long userId) {
        Optional<User> userOptional = userRepository.getById(userId);
        if (userOptional.isAbsent()) {
            throw new InvalidInputException("User not found");
        }

        if (isSystemAdmin(userId)) {
            return userRepository.findAll();
        }

        Long accountId = userOptional.get().getAccount().getId();
        return userRepository.findByAccountId(accountId);
    }


    public boolean isSystemAdmin() {
        return isSystemAdmin(identity.getIdentityDto().getId());
    }

    public boolean isAccountAdmin(Long userId) {
        List<UserRole> userRoleList = userRoleRepository.findByUserId(userId);
        for (UserRole userRole : userRoleList) {
            if (userRole.getRole().getId().equals(roleAccountAdmin)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSystemAdmin(Long userId) {
        List<UserRole> userRoleList = userRoleRepository.findByUserId(userId);
        for (UserRole userRole : userRoleList) {
            if (userRole.getRole().getId().equals(roleSystemAdmin)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasRoleTracker(Long userId) {
        List<UserRole> userRoleList = userRoleRepository.findByUserId(userId);
        for (UserRole userRole : userRoleList)
            if (userRole.getRole().getId().equals(roleTracker))
                return true;
        return false;
    }

    public boolean isLinkedUser(Long userId, String uniqueId) {
        if (hasRoleTracker(userId)) {
        }
        return false;
    }

    public Boolean isChangingUserName(User detachedUser, User userOptional) {
        return detachedUser.getUsername().equals(userOptional.getUsername());
    }

    public Boolean isChangingUserRole(User detachedUser, User user) {
        Role roleDetached = userRoleRepository.getByUserId(detachedUser.getId()).get().getRole();
        Role roleOptional = userRoleRepository.getByUserId(user.getId()).get().getRole();
        return roleDetached.getId().equals(roleOptional.getId());
    }
}
