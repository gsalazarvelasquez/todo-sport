package bo.com.salazargonzalo.module.todosport.rest.sale;

import bo.com.salazargonzalo.module.todosport.app.customer.model.Customer;
import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
class SaleDto implements Serializable {
    private Date date;
    private Long customerId;
    private String total;

    Sale convertToAddSale(SaleDto saleDto) {
        Sale sale = new Sale();
        sale.setDate(saleDto.getDate());
        sale.setTotal(saleDto.getTotal());
        sale.setCustomer(new Customer(saleDto.getCustomerId()));
        return sale;
    }

    Sale convertToUpdateSale(Sale sale, SaleDto saleDto) {
        sale.setTotal(saleDto.getTotal());
        sale.setCustomer(new Customer(saleDto.getCustomerId()));
        return sale;
    }
}
