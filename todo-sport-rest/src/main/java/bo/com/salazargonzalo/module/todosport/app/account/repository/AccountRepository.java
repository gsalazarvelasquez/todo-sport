package bo.com.salazargonzalo.module.todosport.app.account.repository;

import bo.com.salazargonzalo.domain.enumerator.SellerEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.shared.utils.LongUtil;
import bo.com.salazargonzalo.module.shared.utils.StringUtil;
import bo.com.salazargonzalo.module.todosport.app.account.model.Account;

import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class AccountRepository extends Repository {

    public List<Account> findAll() {
        return findAll(Account.class);
    }

    public Optional<Account> getById(final Long id) {
        return LongUtil.isEmpty(id) ? Optional.absent() : getById(Account.class, id);
    }

    public Optional<Account> getByEmail(String email) {
        Map<String, Object> filter = new HashMap<>();
        filter.put("email", email);
        return getBy(Account.class, filter);
    }

    public Optional<Account> getByName(final String name) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("name", name);
        return StringUtil.isEmpty(name) ? Optional.absent() : getBy(Account.class, filters);
    }

    public List<Account> findBySeller(SellerEnum seller) {
        Map<String, Object> filter = new HashMap<>();
        filter.put("seller", seller);
        return findBy(Account.class, filter);
    }
}
