package bo.com.salazargonzalo.module.todosport.app.shoesize.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ShoeSizeNotFoundException extends RuntimeException {
}
