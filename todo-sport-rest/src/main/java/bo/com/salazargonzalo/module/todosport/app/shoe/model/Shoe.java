package bo.com.salazargonzalo.module.todosport.app.shoe.model;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "SHOES")
@Getter
@Setter
@ToString
public class Shoe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SHOE_ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "PHOTO")
    private String photo;

    @Column(name = "CONTAINER")
    private String container;

    @Column(name = "SIZE")
    private String size;

    @Column(name = "PAR")
    private Integer par;

    @Column(name = "DOZEN")
    private String dozen;

    @Column(name = "PRICE")
    private String price;

    @Column(name = "TOTAL")
    private String total;

    @Column(name = "STOCK_PAR")
    private Integer stockPar;

    @Column(name = "STOCK_DOZEN")
    private String stockDozen;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    public Shoe() {
    }

    public Shoe(Long id) {
        this.id = id;
    }
}
