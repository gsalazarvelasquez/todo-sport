package bo.com.salazargonzalo.module.todosport.service;


import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoe.exception.ShoeNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoe.model.Shoe;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import java.io.IOException;
import java.util.List;

public interface IShoeService {

    Shoe add(Shoe shoe);

    List<Shoe> findAll();

    void update(Shoe shoe) throws ShoeNotFoundException;

    Shoe getById(Long id) throws ShoeNotFoundException;

    List<String> findContainersByCode(String code) throws ShoeNotFoundException;

    List<String> findSizesByCodeAndContainer(String code, String container) throws ShoeNotFoundException;

    Shoe getByCodeContainerAndSize(String code, String container, String size) throws ShoeNotFoundException;

    void updateStockBySaleId(Long id) throws SaleNotFoundException;

    void delete(Long id) throws ShoeNotFoundException;

    void uploadShoePhoto(Long id, MultipartFormDataInput input) throws ShoeNotFoundException, IOException;

    void importShoesExcelFile(MultipartFormDataInput input) throws IOException, InvalidFormatException;
}
