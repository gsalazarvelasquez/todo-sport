package bo.com.salazargonzalo.module.shared.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class EmailAlreadyExistException extends RuntimeException {
}
