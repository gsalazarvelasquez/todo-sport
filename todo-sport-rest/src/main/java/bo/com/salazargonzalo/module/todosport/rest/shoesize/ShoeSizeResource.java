package bo.com.salazargonzalo.module.todosport.rest.shoesize;

import bo.com.salazargonzalo.module.shared.rest.security.annotations.LoggedIn;
import bo.com.salazargonzalo.module.todosport.app.container.exception.ContainerNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoesize.exception.ShoeSizeNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.shoesize.model.ShoeSize;
import bo.com.salazargonzalo.module.todosport.service.IShoeSizeService;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("ShoeSizes")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Stateless
public class ShoeSizeResource {

    @Inject
    private Logger log;

    @Inject
    public IShoeSizeService shoeSizeService;

    @POST
    @LoggedIn
    public Response addShoeSize(ShoeSize shoeSize) {
        try {
            shoeSize = shoeSizeService.add(shoeSize);
            return Response
                    .ok()
                    .entity(shoeSize.getId())
                    .build();
        } catch (Exception e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @GET
    public Response getShoeSizeList() {
        try {
            final List<ShoeSize> shoeSizeList = shoeSizeService.findAll();
            return Response
                    .ok()
                    .entity(shoeSizeList)
                    .build();

        } catch (RuntimeException e) {
            log.error("[ERROR] RuntimeException: " + e.getMessage());
            return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();

        } catch (Exception e) {
            log.error("[ERROR] Exception: " + e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @PUT
    @LoggedIn
    public Response updateShoeSize(ShoeSize shoeSize) {
        try {
            shoeSizeService.update(shoeSize);
            return Response
                    .ok()
                    .build();
        } catch (ContainerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteShoeSize(@PathParam("id") Long id) {
        try {
            shoeSizeService.delete(id);
            return Response
                    .ok()
                    .build();
        } catch (ShoeSizeNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @LoggedIn
    @Path("/{id}")
    public Response getShoeSizeById(@PathParam("id") Long id) {
        log.info("param: " + id);
        try {
            ShoeSize shoeSize = shoeSizeService.getById(id);
            return Response
                    .ok()
                    .entity(shoeSize)
                    .build();
        } catch (ContainerNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}