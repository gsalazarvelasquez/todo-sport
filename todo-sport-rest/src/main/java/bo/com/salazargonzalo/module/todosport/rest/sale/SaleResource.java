package bo.com.salazargonzalo.module.todosport.rest.sale;

import bo.com.salazargonzalo.module.shared.rest.security.annotations.LoggedIn;
import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.sale.model.Sale;
import bo.com.salazargonzalo.module.todosport.service.ISaleService;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("Sales")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Stateless
public class SaleResource {

    @Inject
    private Logger log;

    @Inject
    public ISaleService saleService;


    @POST
    @LoggedIn
    public Response addSale(SaleDto saleDto) {
        log.info("add Size: " + saleDto);
        try {
            Sale sale = saleDto.convertToAddSale(saleDto);
            sale = saleService.add(sale);
            return Response
                    .ok()
                    .entity(sale.getId())
                    .build();
        } catch (Exception e) {
            log.info(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @PUT
    @LoggedIn
    @Path("{id}")
    public Response updateSale(@PathParam("id") Long id, SaleDto saleDto) {
        log.info("Update saleId: " + id);
        log.info("Update sale: " + saleDto);
        try {

            Sale sale = saleService.getById(id);
            sale = saleDto.convertToUpdateSale(sale, saleDto);
            saleService.update(sale);
            return Response
                    .ok()
                    .build();
        } catch (SaleNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        } catch (Exception e) {
            log.error("error: " + e.getMessage());
            return Response.status(Status.BAD_REQUEST).build();
        }
    }


    @GET
    @LoggedIn
    @Path("/{id}")
    public Response getSizeById(@PathParam("id") Long id) {
        log.info("param: " + id);
        try {
            Sale sale = saleService.getById(id);
            return Response
                    .ok()
                    .entity(sale)
                    .build();
        } catch (SaleNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @LoggedIn
    public Response getAllSales() {
        try {
            List<Sale> sales = saleService.findAll();
            return Response
                    .ok()
                    .entity(sales)
                    .build();
        } catch (Exception e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removeSale(@PathParam("id") Long id) {
        try {
            saleService.remove(id);
            return Response
                    .ok()
                    .build();
        } catch (SaleNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}