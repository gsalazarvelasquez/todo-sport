package bo.com.salazargonzalo.module.todosport.rest.salesdetail;

import bo.com.salazargonzalo.module.shared.rest.security.annotations.LoggedIn;
import bo.com.salazargonzalo.module.todosport.app.sale.exception.SaleNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.saledetail.model.SaleDetail;
import bo.com.salazargonzalo.module.todosport.service.ISalesDetailService;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("SalesDetails")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Stateless
public class SalesDetailResource {

    @Inject
    private Logger log;

    @Inject
    public ISalesDetailService salesDetailService;

    @POST
    @LoggedIn
    public Response addSaleDetail(List<SaleDetailDto> saleDetailDtoList) {
        try {
            List<SaleDetail> saleDetails = SaleDetailDto.toSaleDetailList(saleDetailDtoList);
            salesDetailService.add(saleDetails);
            return Response
                    .ok()
                    .build();
        } catch (Exception e) {
            log.info(e.getMessage(), e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("query")
    public Response findSalesDetailBySaleId(@QueryParam("saleId") Long saleId) {
        log.info("param: " + saleId);
        try {
            List<SaleDetail> saleDetails = salesDetailService.findBySaleId(saleId);
            return Response
                    .ok()
                    .entity(saleDetails)
                    .build();
        } catch (SaleNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response removeSaleDetail(@PathParam("id") Long id) {
        try {
            salesDetailService.delete(id);
            return Response
                    .ok()
                    .build();
        } catch (SaleNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("query")
    public Response deleteBySaleId(@QueryParam("saleId") Long saleId) {
        try {
            salesDetailService.deleteBySaleId(saleId);
            return Response
                    .ok()
                    .build();
        } catch (SaleNotFoundException e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}