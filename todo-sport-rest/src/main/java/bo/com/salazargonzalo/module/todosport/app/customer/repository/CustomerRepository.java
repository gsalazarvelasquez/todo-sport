package bo.com.salazargonzalo.module.todosport.app.customer.repository;

import bo.com.salazargonzalo.domain.enumerator.StatusEnum;
import bo.com.salazargonzalo.module.shared.persistence.Optional;
import bo.com.salazargonzalo.module.shared.persistence.Repository;
import bo.com.salazargonzalo.module.todosport.app.customer.model.Customer;

import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class CustomerRepository extends Repository {

    public List<Customer> findAll() {
        return findAll(Customer.class);
    }

    public List<Customer> findAllEnabled() {
        Map<String, Object> filter = new HashMap<>();
        filter.put("status", StatusEnum.ENABLED);
        return findBy(Customer.class, filter);
    }

    public Optional<Customer> findById(final Long id) {
        return getById(Customer.class, id);
    }

    public Optional<Customer> getByName(final String name) {
        Map<String, Object> filter = new HashMap<>();
        filter.put("name", name);
        filter.put("status", StatusEnum.ENABLED);
        return getBy(Customer.class, filter);
    }

    public void deleteAll() {
        delete(findAll());
    }
}
