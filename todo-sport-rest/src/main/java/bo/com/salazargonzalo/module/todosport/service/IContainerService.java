package bo.com.salazargonzalo.module.todosport.service;


import bo.com.salazargonzalo.module.todosport.app.container.exception.ContainerNotFoundException;
import bo.com.salazargonzalo.module.todosport.app.container.model.Container;

import java.util.List;

public interface IContainerService {

    Container add(Container container);

    void update(Container container) throws ContainerNotFoundException;

    Container getById(Long id) throws ContainerNotFoundException;

    void delete(Long id) throws ContainerNotFoundException;

    List<Container> findAll();


}
