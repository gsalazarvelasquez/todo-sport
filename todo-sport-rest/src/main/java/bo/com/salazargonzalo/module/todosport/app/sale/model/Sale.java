package bo.com.salazargonzalo.module.todosport.app.sale.model;

import bo.com.salazargonzalo.domain.enumerator.SaleStatusEnum;
import bo.com.salazargonzalo.module.todosport.app.customer.model.Customer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SALES")
@Getter
@Setter
@ToString
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SALE_ID")
    private Long id;

    @Column(name = "DATE")
    private Date date;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CUSTOMER_ID",
            foreignKey = @ForeignKey(name = "FK_SALE_CUSTOMER_ID"))
    private Customer customer;

    @Column(name = "TOTAL")
    private String total;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private SaleStatusEnum status;

    public Sale() {
    }

    public Sale(Long id) {
        this.id = id;
    }
}
