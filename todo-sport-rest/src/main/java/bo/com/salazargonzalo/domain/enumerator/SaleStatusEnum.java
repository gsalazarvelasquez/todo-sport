package bo.com.salazargonzalo.domain.enumerator;

import java.io.Serializable;

public enum SaleStatusEnum implements Serializable {
    PAID("PAGADO"),
    CANCELED("ANULADO");

    private String label;

    SaleStatusEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
