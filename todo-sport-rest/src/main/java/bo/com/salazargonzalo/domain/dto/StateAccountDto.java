package bo.com.salazargonzalo.domain.dto;

import bo.com.salazargonzalo.module.shared.utils.EntityUtil;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
public class StateAccountDto implements Serializable {

    private String deviceName;
    private String endSubscription;


    public static StateAccountDto createNew() {
        StateAccountDto stateAccountDto = new StateAccountDto();
        stateAccountDto.deviceName = EntityUtil.DEFAULT_STRING;
        stateAccountDto.endSubscription = EntityUtil.DEFAULT_STRING;
        return stateAccountDto;
    }
}
