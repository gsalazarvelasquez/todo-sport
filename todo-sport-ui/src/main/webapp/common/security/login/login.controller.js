(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('AppController', AppController)
        .controller('LoginController', LoginController)
        .controller('ErrorController', ErrorController);

    function AppController($scope, $rootScope, $window, dataService, securityService, $location, $interval, $route) {
        init();
        $scope.getClass = getClass;
        $scope.logout = logout;
        $scope.hasRole = hasRole;
        $scope.changePasswordUser = changePasswordUser;
        $scope.arePasswordsEquals = arePasswordsEquals;
        $scope.isBtnChangePasswordActive = isBtnChangePasswordActive;
        $scope.reset = reset;
        $scope.$on('$viewContentLoaded', function (event) {
        });
        $scope.$on('$locationChangeStart', function (event, next) {

            $scope.appConfig.headerVisible = true;

            var page = $location.path().substring(1, $location.path().length);

            if (contains(page, 'error')) {
                return;
            }

            if (angular.equals(page, 'forgot-password')
                || angular.equals(page, 'login')
                || contains(page, 'reset-password')) {
                if (securityService.isLoggedIn()) {
                    $rootScope.goToPage("/");
                }
            } else if (securityService.isLoggedIn()) {
                //console.log('$scope.hasRole ', $scope.hasRole());
                if (!$scope.hasRole(page)) {
                    if ($scope.hasRole("account-management")) {
                        $rootScope.goToPage("/account-management")

                    } else {
                        $rootScope.goToPage("/");
                    }
                }
            } else {
                securityService.endSession();
                $scope.currentUser = {};
                $rootScope.goToPage("/login");
            }
            $scope.currentUser = securityService.getUser();
        });

        function getClass(path) {
            if ($location.path().substr(0, path.length) === path) {
                return "open-menu"
            } else {
                return ""
            }
        }

        function init() {
            $scope.loginError = false;
            $scope.isEnabled = false;
            $scope.account = {};
            $scope.tMspinner = {};
            $scope.tMspinner.WaitingForPayment = false;
            $scope.isLoggedIn = securityService.isLoggedIn();

            if ($scope.isLoggedIn) {
                $scope.currentUser = securityService.getUser();
            }
        }

        function logout() {
            securityService.endSession();
            $rootScope.goToPage("/login");
        }

        function hasRole(role) {
            if (!$scope.currentUser) {
                return false;
            }
            return $scope.currentUser.permission.indexOf(role) > -1;
        }

        function contains(str, value) {
            return str.indexOf(value) !== -1;
        }

        function changePasswordUser(userSecurity) {
            dataService.changePasswordUser(userSecurity)
                .then(function (data) {
                    //console.log("data: ", data);
                    setSuccessMessage("Contraseña modificada correctamente.");
                    cleanUser();
                }, function (error) {
                    //console.log("error: ", error);
                    setErrorMessage("Error: " + error.data);
                    cleanUser();
                });
        }

        function arePasswordsEquals(form) {
            return angular.equals(form.password.$modelValue, form.passwordConfirm.$modelValue);
        }

        function isBtnChangePasswordActive(form) {
            if (!form.$valid) {
                return true;
            }

            return !$scope.arePasswordsEquals(form);
        }

        function reset() {
            dataService.resetSystemData().then(function (resp) {
                $route.reload();
                setSuccessMessage("System Reset!")
            })
        }

        function cleanUser() {
            $scope.user = {};
        }
    }

    function LoginController($scope, $rootScope, dataService, securityService, $timeout, onlineStatus) {
        //console.log("loginCtrl");
        $scope.credentials = {};
        $scope.registerCredentials = {};
        $scope.currentUser = {};
        $scope.appConfig.headerVisible = false;

        $scope.onlineStatus = onlineStatus;

        $scope.$watch('onlineStatus.isOnline()', function (online) {
            $scope.connexionError = !online;
        });

        $scope.login = function () {
            setTimeout(makeLogin(), 7000);
        };

        function makeLogin() {
            dataService.login($scope.credentials)
                .success(function (account) {
                    // console.log("[LOGIN] -> ", account);
                    securityService.initSession(account);
                    $scope.credentials = account;
                    $rootScope.goToPage("/");
                })
                .error(function (data, status) {
                    console.log('error: ', status);
                    if (status == '401') {
                        $scope.loginError = true;
                        $timeout(loginError, 3000);
                    } else if (status == '405') {
                        $scope.connexionError = true;
                        $timeout(connexionError, 3000);
                    }
                });
        }

        function connexionError() {
            $scope.connexionError = false;
        }

        function loginError() {
            $scope.loginError = false;
        }

        $scope.register = function (form) {
            if (form.$valid) {
                makeRegister();
            }
        };

        function makeRegister() {
            $('.modal').css("z-index", "10 !important");
            if ($scope.registerCredentials.password != $scope.registerCredentials.passwordConfirm) {
                setErrorMessage("Confirmar contraseña, no coincide");
            } else if (Object.keys($scope.registerCredentials).length == 5) {
                dataService.register($scope.registerCredentials).then(function (account) {
                    console.log("register ---> ", account)
                    securityService.initSession(account);
                    $scope.credentials = account;
                    $rootScope.goToPage("/");
                    $scope.getShoppingCartItems();
                }, function (error) {
                    //console.log("createSubscription error");
                    setErrorMessage("Error " + error.data)

                })
            }
        }

        $scope.cleanForm = function (form) {
            $scope.registerCredentials.name = null;
            $scope.registerCredentials.email = "";
            $scope.registerCredentials.username = null;
            $scope.registerCredentials.password = null;
            $scope.registerCredentials.passwordConfirm = null;
            form.$setPristine();
        }
    }

    function ErrorController($scope) {
        $scope.appConfig.headerVisible = false;
    }

})();

var setSuccessMessage = function (message, isLoggedIn) {
    // create the notification
    console.log('into login.controller.js');
    var notification = new NotificationFx({
        message: '<span class="icon fa fa-check-circle fa-fw fa-lg fa-2x"></span><p>' + message + '</p>',
        layout: 'bar',
        effect: 'slidetop',
        type: 'success',
        isloggedIn: isLoggedIn != undefined ? isLoggedIn : true
    });
    // show the notification
    notification.show();
};

var setErrorMessage = function (message, isLoggedIn) {
    // create the notification
    var notification = new NotificationFx({
        message: '<span class="icon fa fa-times-circle fa-fw fa-lg fa-2x"></span><p>' + message + '</p>',
        layout: 'bar',
        effect: 'slidetop',
        type: 'error',
        isloggedIn: isLoggedIn != undefined ? isLoggedIn : true
    });
    // show the notification
    notification.show();
};

var setWarningMessage = function (message, isLoggedIn) {
    // create the notification
    var notification = new NotificationFx({
        message: '<span class="icon fa fa-exclamation-triangle fa-fw fa-lg fa-2x"></span><p>' + message + '</p>',
        layout: 'bar',
        effect: 'slidetop',
        type: 'warning',
        isloggedIn: isLoggedIn != undefined ? isLoggedIn : true
    });
    // show the notification
    notification.show();
};

var setAlertDownSuccess = function (message) {
    var btnSlide = document.getElementById('notification-trigger-slide');

    btnSlide.disabled = false;

    classie.add(btnSlide, 'active');

    classie.remove(btnSlide, 'active');

    // create the notification
    var notification = new NotificationFx({
        message: '<p>' + message + '</p>',
        layout: 'growl',
        effect: 'slide',
        type: 'notice', // notice, warning or error
        onClose: function () {
            btnSlide.disabled = false;
        }
    });
    notification.show();
    this.disabled = true;
};




