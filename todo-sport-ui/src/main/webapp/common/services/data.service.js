(function () {

    'use strict';

    angular
        .module('todo-sport')
        .factory('dataService', dataService)
        .factory('onlineStatus', onlineStatus);

    function dataService($http, $location) {
        var domainUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/";
        var rootRestfulPath = domainUrl + 'rest/';
        //var rootRestfulPath = 'http://192.168.0.141:8080/rest/';

        return {
            login: login,
            passwordRecovery: passwordRecovery,
            getPasswordToken: getPasswordToken,
            findAllAccountAndDeviceList: findAllAccountAndDeviceList,
            findAllStatusPayment: findAllStatusPayment,
            resetPassword: resetPassword,
            resetSystemData: resetSystemData,
            appTest: appTest,
            // Shoes
            findShoeContainersByCode: findShoeContainersByCode,
            findShoeSizesByCodeAndContainer: findShoeSizesByCodeAndContainer,
            getShoeByCodeContainerAndSize: getShoeByCodeContainerAndSize,
            getAllShoes: getAllShoes,
            updateShoeStock: updateShoeStock,
            deleteShoe: deleteShoe,
            uploadPhoto: uploadPhoto,
            importExcelShoe: importExcelShoe,
            updateShoe: updateShoe,
            // Sale
            addSale: addSale,
            updateSale: updateSale,
            getAllSales: getAllSales,
            getSaleById: getSaleById,
            cancelSale: cancelSale,
            // Sale Detail
            addSaleDetails: addSaleDetails,
            findSaleDetailBySaleId: findSaleDetailBySaleId,
            deleteSaleDetailBySaleId: deleteSaleDetailBySaleId,

            // Customer
            addCustomer: addCustomer,
            updateCustomer: updateCustomer,
            removeCustomer: removeCustomer,
            getAllCustomers: getAllCustomers,

            // Shoe Sizes
            getAllShoeSizes: getAllShoeSizes,
            createShoeSize: createShoeSize,
            updateShoeSize: updateShoeSize,
            deleteShoeSize: deleteShoeSize,

            // Container
            getAllContainers: getAllContainers,
            createContainer: createContainer,
            updateContainer: updateContainer,
            deleteContainer: deleteContainer,


            findFreeDevices: function () {
                return $http.get(rootRestfulPath + 'device/backend/list/free');
            },

            findDevicesByAccountId: function (accountId) {
                return $http.get(rootRestfulPath + 'device/backend/list/' + accountId);
            },

            updateDeviceAssignment: function (devices, accountId) {
                return $http.put(rootRestfulPath + 'device/backend/update/' + accountId, devices)
                    .then(function (response) {
                        return response.data;
                    });
            },

            findAllAccounts: function () {
                return $http.get(rootRestfulPath + 'accounts');
            },

            createAccount: function (account) {
                return $http.post(rootRestfulPath + 'accounts', account).then(function (response) {
                    return response.data;
                });
            },

            resetPasswordUser: function (user) {
                return $http.put(rootRestfulPath + 'users/reset/password', user).then(function (response) {
                    return response.data;
                });
            },

            updateAccount: function (id, account) {
                return $http.put(rootRestfulPath + 'accounts/' + id, account).then(function (response) {
                    return response.data;
                });
            },

            deleteAccount: function (id) {
                return $http.delete(rootRestfulPath + 'accounts/' + id).then(function (response) {
                    return response.data;
                });
            },

            deleteUser: function (userId) {
                return $http.delete(rootRestfulPath + 'users/' + userId).then(function (response) {
                    return response.data;
                });
            },

            changePasswordUser: function (userSecurity) {
                return $http.put(rootRestfulPath + 'users/password', userSecurity).then(function (response) {
                    return response.data;
                });
            },

            changeAdminPasswordUser: function (userSecurity) {
                return $http.put(rootRestfulPath + 'users/reset/password', userSecurity)
                    .then(function (response) {
                        return response.data;
                    });
            },

            sendMessage: function (contact) {
                return $http.post(rootRestfulPath + 'contact/send-mail', contact).then(function (response) {
                    return response.data;
                });
            },

            getSubscriptionsList: function () {
                return $http.get(rootRestfulPath + 'subscription/management/list');
            },

            getShoppingCartItems: function () {
                return $http.get(rootRestfulPath + "subscription/shopping-cart/items");
            },

            requestStatusPayment: function (requestId) {
                return $http.get(rootRestfulPath + "subscription/pay/status/" + requestId);
            },

            findBillStatus: function () {
                return $http.get(rootRestfulPath + "accounts/info");
            },

            updateBillStatus: function (billStatus) {
                return $http.put(rootRestfulPath + "accounts/info", billStatus).then(function (response) {
                    return response.data;
                });
            },

            paymentHistorial: function () {
                return $http.get(rootRestfulPath + "payments");
            },

            register: function (registerFrom) {
                return $http.post(rootRestfulPath + 'authenticate/create/web/account', registerFrom)
                    .then(function (response) {
                        return response.data;
                    });
            },

            deletePaymentHistorial: function (historials) {
                return $http.put(rootRestfulPath + 'payments/', historials).then(function (response) {
                    return response.data;
                });
            },

            findTransaction: function (accountId, deviceId, status) {
                return $http.get(rootRestfulPath + 'payments/' + accountId + '/' + deviceId + '/' + status);
            }
        };

        function login(credentials) {
            var config = {
                url: rootRestfulPath + "authenticate",
                method: 'post',
                data: credentials,
                headers: {
                    "Content-Type": "application/x-authc-username-password-web+json;application/json;charset=UTF-8"
                }
            };
            return $http(config);
        }

        function passwordRecovery(token) {
            return $http.get(rootRestfulPath + "authenticate/password-token/" + token);
        }

        function getPasswordToken(token) {
            return $http.get(rootRestfulPath + "authenticate/password-token/" + token);
        }

        function findAllAccountAndDeviceList() {
            return $http.get(rootRestfulPath + "accounts/transactions");
        }

        function findAllStatusPayment() {
            return $http.get(rootRestfulPath + "accounts/transactions/status-payment");
        }

        function resetPassword(passwordtoken) {
            return $http.put(rootRestfulPath + "authenticate/reset-password", passwordtoken)
                .then(function (response) {
                    return response.data;
                });
        }

        function resetSystemData() {
            return $http.post(rootRestfulPath + 'App/reset', {}).then(function (response) {
                return response.data;
            });
        }

        function appTest() {
            return $http.get(rootRestfulPath + "App/test");
        }

        function getAllShoes() {
            return $http.get(rootRestfulPath + 'Shoes');
        }

        function updateShoeStock(saleId) {
            return $http.put(rootRestfulPath + "Shoes/updateStock/query?saleId=" + saleId)
                .then(function (response) {
                    return response.data;
                });
        }

        function deleteShoe(id) {
            return $http.delete(rootRestfulPath + 'Shoes/' + id).then(function (response) {
                return response.data;
            })
        }

        function uploadPhoto(id, data) {
            var url = rootRestfulPath + 'Shoes/' + id + '/uploadPhoto';
            return $http.put(url, data, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (res) {
                return res.data;
            });
        }

        function importExcelShoe(data) {
            var url = rootRestfulPath + 'Shoes/importExcel';
            return $http.post(url, data, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (res) {
                return res.data;
            });
        }

        function updateShoe(shoe) {
            return $http.put(rootRestfulPath + "Shoes/", shoe).then(function (response) {
                return response.data;
            });
        }

        function findShoeContainersByCode(code) {
            return $http.get(rootRestfulPath + 'Shoes/containers/query?code=' + code);
        }

        function findShoeSizesByCodeAndContainer(code, container) {
            return $http.get(rootRestfulPath + 'Shoes/sizes/query?code=' + code + "&container=" + container);
        }

        function getShoeByCodeContainerAndSize(code, container, size) {
            return $http.get(rootRestfulPath + 'Shoes/query?code=' + code + "&container=" + container + "&size=" + size);
        }

        function getAllCustomers() {
            return $http.get(rootRestfulPath + 'Customers');
        }

        function getAllShoeSizes() {
            return $http.get(rootRestfulPath + 'ShoeSizes');
        }

        function createShoeSize(shoeSize) {
            return $http.post(rootRestfulPath + 'ShoeSizes', shoeSize).then(function (response) {
                return response.data;
            });
        }

        function updateShoeSize(shoeSize) {
            return $http.put(rootRestfulPath + "ShoeSizes/", shoeSize)
                .then(function (response) {
                    return response.data;
                });
        }

        function deleteShoeSize(id) {
            return $http.delete(rootRestfulPath + 'ShoeSizes/' + id)
                .then(function (response) {
                    return response.data;
                })
        }

        function getAllContainers() {
            return $http.get(rootRestfulPath + 'Containers');
        }

        function createContainer(container) {
            return $http.post(rootRestfulPath + 'Containers', container)
                .then(function (response) {
                    return response.data;
                });
        }

        function updateContainer(container) {
            return $http.put(rootRestfulPath + "Containers/", container)
                .then(function (response) {
                    return response.data;
                });
        }

        function deleteContainer(id) {
            return $http.delete(rootRestfulPath + 'Containers/' + id)
                .then(function (response) {
                    return response.data;
                })
        }

        function addCustomer(customer) {
            return $http.post(rootRestfulPath + 'Customers/', customer)
                .then(function (response) {
                    return response.data;
                });
        }

        function getAllSales() {
            return $http.get(rootRestfulPath + 'Sales');
        }

        function getSaleById(id) {
            return $http.get(rootRestfulPath + 'Sales/' + id);
        }

        function cancelSale(id) {
            return $http.delete(rootRestfulPath + 'Sales/' + id)
                .then(function (response) {
                    return response.data;
                })
        }

        function addSale(sale) {
            return $http.post(rootRestfulPath + 'Sales/', sale)
                .then(function (response) {
                    return response.data;
                });
        }

        function updateSale(id, sale) {
            return $http.put(rootRestfulPath + 'Sales/' + id, sale)
                .then(function (response) {
                    return response.data;
                });

        }

        function addSaleDetails(saleDetails) {
            return $http.post(rootRestfulPath + 'SalesDetails', saleDetails)
                .then(function (response) {
                    return response.data;
                });
        }

        function findSaleDetailBySaleId(saleId) {
            return $http.get(rootRestfulPath + 'SalesDetails/query?saleId=' + saleId);
        }

        function deleteSaleDetailBySaleId(id) {
            return $http.delete(rootRestfulPath + 'SalesDetails/query?saleId=' + id)
                .then(function (response) {
                    return response.data;
                })
        }

        function updateCustomer(id, customer) {
            return $http.put(rootRestfulPath + 'Customers/' + id, customer)
                .then(function (response) {
                    return response.data;
                });
        }

        function removeCustomer(id) {
            return $http.delete(rootRestfulPath + 'Customers/' + id)
                .then(function (response) {
                    return response.data;
                })
        }
    }

    function onlineStatus($window, $rootScope) {
        var onlineStatus = {};

        onlineStatus.onLine = $window.navigator.onLine;

        onlineStatus.isOnline = function () {
            return onlineStatus.onLine;
        };

        $window.addEventListener("online", function () {
            onlineStatus.onLine = true;
            $rootScope.$digest();
        }, true);

        $window.addEventListener("offline", function () {
            onlineStatus.onLine = false;
            $rootScope.$digest();
        }, true);

        return onlineStatus;
    }
})();




