(function () {

    'use strict';

    angular
        .module('todo-sport')
        .constant('buhoConfig', {
            "buhoMapStyle": [{
                "featureType": "landscape",
                "stylers": [{"hue": "#FFBB00"}, {"saturation": 43.400000000000006}, {"lightness": 37.599999999999994}, {"gamma": 1}]
            }, {
                "featureType": "road.highway",
                "stylers": [{"hue": "#FFC200"}, {"saturation": -61.8}, {"lightness": 45.599999999999994}, {"gamma": 1}]
            }, {
                "featureType": "road.arterial",
                "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 51.19999999999999}, {"gamma": 1}]
            }, {
                "featureType": "road.local",
                "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 52}, {"gamma": 1}]
            }, {
                "featureType": "water",
                "stylers": [{"hue": "#0078FF"}, {"saturation": -13.200000000000003}, {"lightness": 2.4000000000000057}, {"gamma": 1}]
            }, {
                "featureType": "poi",
                "stylers": [{"hue": "#00FF6A"}, {"saturation": -1.0989010989011234}, {"lightness": 11.200000000000017}, {"gamma": 1}]
            }],
            "latitudeMap": -17.8000,
            "longitudeMap": -63.1833,
            "dateFormat": 'DD-MM-YYYY',
            "timeOutCurrentPosition": 20000, // ms
            "buhoMapStyleOld": [{"stylers": [{"hue": "#00A6F9"}, {"gamma": 1}]}],
            "zoomRanges": [550000, 500000, 450000, 350000, 250000, 150000, 85000, 40000, 20000, 15000, 9000, 5000, 2000, 1000, 600, 350, 100, 50, 20, 10, 5, 1]
        })
    ;
})();



