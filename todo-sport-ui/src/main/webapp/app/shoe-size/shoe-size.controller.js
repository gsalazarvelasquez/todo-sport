(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('ShoeSizeController', ShoeSizeController);

    function ShoeSizeController($scope, $modal, $window, dataService) {

        init();
        $scope.selectShoeSize = selectShoeSize;
        $scope.updateShoeSize = updateShoeSize;
        $scope.addShoeSize = addShoeSize;
        $scope.deleteShoeSize = deleteShoeSize;
        $scope.cancelShoeSizeModal = cancelShoeSizeModal;


        function selectShoeSize(shoeSize) {
            $scope.shoeSize = shoeSize;
        }

        function updateShoeSize() {
            dataService.updateShoeSize($scope.shoeSize).then(function (value) {
                init();
            })
        }

        function addShoeSize() {
            dataService.createShoeSize($scope.shoeSize).then(function (value) {
                init();
            })
        }

        function deleteShoeSize() {
            dataService.deleteShoeSize($scope.shoeSize.id).then(function (value) {
                init();
            })
        }

        function cancelShoeSizeModal() {
            init();
        }

        function init() {
            $scope.shoeSize = {};
            dataService.getAllShoeSizes().success(function (resp) {
                console.log('shoe sizes: ', resp);
                $scope.shoeSizes = resp;
            });
        }
    }
})();