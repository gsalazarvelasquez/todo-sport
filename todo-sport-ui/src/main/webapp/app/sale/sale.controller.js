(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('SaleController', SaleController);


    function SaleController($scope, $location, $interval, $window, dataService) {

        init();
        $scope.showSale = showSale;
        $scope.editSale = editSale;
        $scope.selectItem = selectItem;
        $scope.cancelSale = cancelSale;

        function init() {
            getAllSales();
        }

        function showSale(saleId) {
            $location.path('sale-detail').search({saleId: saleId})
        }

        function editSale(saleId) {
            $location.path('edit-sale').search({saleId: saleId})
        }

        function selectItem(sale) {
            $scope.sale = sale;
        }

        function cancelSale() {
            console.log('cancelSale: ', $scope.sale);
            dataService.cancelSale($scope.sale.id).then(function (resp) {
                console.log('response:', resp);
                // Update Shoe Stock
                dataService.updateShoeStock($scope.sale.id).then(function (value) {
                    getAllSales();
                })
            })
        }

        function getAllSales() {
            dataService.getAllSales().success(function (resp) {
                console.log('getAllSales: ', resp);
                $scope.sales = resp;
            })
        }
    }
})();


