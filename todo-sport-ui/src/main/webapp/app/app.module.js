(function () {

    'use strict';

    angular
        .module('todo-sport', [
            'ngRoute',
            'angular-loading-bar',
            'ngAnimate',
            'easypiechart',
            'pascalprecht.translate',
            'ui.bootstrap',
            'SecurityServiceModule',
            'datatables'
        ]);
})();
