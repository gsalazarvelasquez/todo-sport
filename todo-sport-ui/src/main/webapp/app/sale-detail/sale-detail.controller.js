(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('SaleDetailController', SaleDetailController);


    function SaleDetailController($scope, $routeParams, $window, dataService) {

        init();

        function init() {
            $scope.total = 0;
            $scope.items = [];
            var saleId = $routeParams.saleId;
            getSalesDetails(saleId);
            getSale(saleId);
        }

        function getSale(saleId) {
            dataService.getSaleById(saleId).success(function (resp) {
                console.log('getSaleById: ', resp);
                $scope.sale = resp;
            })
        }

        function getSalesDetails(saleId) {
            dataService.findSaleDetailBySaleId(saleId).success(function (data) {
                console.log('findSaleDetailBySaleId: ', data);
                $scope.items = data;
                getTotal();
            });
        }

        function getTotal() {
            $scope.items.forEach(function (item) {
                $scope.total += (+item.price * +item.par);
            })
        }
    }
})();


