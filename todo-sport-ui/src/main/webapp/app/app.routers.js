(function () {

    'use strict';

    angular
        .module('todo-sport')
        .config(['$routeProvider', function ($routeProvider) {

            $routeProvider
                .when("/", {
                    redirectTo: '/sale-management'
                })
                .when("/sale-management", {
                    templateUrl: "app/sale/sale.html",
                    controller: "SaleController",
                    title: 'Ventas'
                })
                .when("/add-sale", {
                    templateUrl: "app/add-sale/add-sale.html",
                    controller: "AddSaleController",
                    title: 'Nueva Venta'
                })
                .when("/edit-sale/:saleId?", {
                    templateUrl: "app/edit-sale/edit-sale.html",
                    controller: "EditSaleController",
                    params: {name: 'saleId'},
                    title: 'Editar Venta'
                })
                .when("/sale-detail/:saleId?", {
                    templateUrl: "app/sale-detail/sale-detail.html",
                    controller: "SaleDetailController",
                    title: 'Detalle de Venta',
                    params: {name: 'saleId'}
                })
                .when("/customer-management", {
                    templateUrl: "app/customer/customer.html",
                    controller: "CustomerController",
                    title: 'Customer Management'
                })
                .when("/shoe-management", {
                    templateUrl: "app/shoe/shoe.html",
                    controller: "ShoeController",
                    title: 'Gestion de Zapatillas'
                })
                .when("/shoe-size-config", {
                    templateUrl: "app/shoe-size/shoe-size.html",
                    controller: "ShoeSizeController",
                    title: 'Lista de Tallas'
                })
                .when("/container-config", {
                    templateUrl: "app/container/container.html",
                    controller: "ContainerController",
                    title: 'Lista de Contenedores'
                })
                .when("/transactions", {
                    templateUrl: "app/admin/transactions/transactions.html",
                    controller: "TransactionsController",
                    title: 'Transactions'
                })
                .when("/error-404", {
                    templateUrl: "common/errors/error-404.html",
                    controller: "errorCtrl",
                    title: 'Page Not Found'
                })
                .when("/error-401", {
                    templateUrl: "common/errors/error-401.html",
                    controller: "errorCtrl",
                    title: 'Page Not Authorized'
                })
                .otherwise({
                    redirectTo: '/error-404',
                    templateUrl: "common/errors/error-404.html"
                });
        }]);
})();
