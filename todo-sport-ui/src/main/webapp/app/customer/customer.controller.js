(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('CustomerController', CustomerController);

    function CustomerController($scope, $modal, $window, dataService) {

        init();

        $scope.addCustomer = addCustomer;
        $scope.editCustomer = editCustomer;
        $scope.updateCustomer = updateCustomer;
        $scope.removeCustomer = removeCustomer;

        function init() {
            $scope.customer = {fullName: '', phoneNumber: '', address: ''};
            getAllCustomer();
        }

        function addCustomer() {
            console.log('addCustomer: ', $scope.customer);
            dataService.addCustomer($scope.customer).then(function (resp) {
                console.log('response:', resp);
                getAllCustomer();
            })
        }

        function editCustomer(customer) {
            console.log('editCustomer: ', $scope.customer);
            $scope.customer = customer;
        }

        function updateCustomer() {
            console.log('updateCustomer: ', $scope.customer);
            dataService.updateCustomer($scope.customer.id, $scope.customer).then(function (resp) {
                console.log('response:', resp);
                getAllCustomer();
            });
        }

        function removeCustomer() {
            console.log('removeCustomer: ', $scope.customer);
            dataService.removeCustomer($scope.customer.id).then(function (resp) {
                console.log('response:', resp);
                getAllCustomer();
            });
        }

        function getAllCustomer() {
            dataService.getAllCustomers().success(function (resp) {
                console.log('customers: ', resp);
                $scope.customers = resp;
            });
        }
    }
})();






