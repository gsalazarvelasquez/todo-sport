(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('AddSaleController', AddSaleController);

    function AddSaleController($scope, $interval, $window, $location, dataService) {

        init();
        $scope.addItem = addItem;
        $scope.removeItem = removeItem;
        $scope.onChangeCode = onChangeCode;
        $scope.onChangeContainer = onChangeContainer;
        $scope.onChangeSize = onChangeSize;
        $scope.onChangePar = onChangePar;
        $scope.onChangeDozen = onChangeDozen;
        $scope.onChangePrice = onChangePrice;
        $scope.processSales = processSales;

        function addItem() {
            if (!isShoeValid())
                return;
            if (isShoeNotFound()) {
                $scope.shoe.index = $scope.items.length;
                $scope.items.push($scope.shoe);
            } else
                updateExistingShoe();

            $scope.shoe = {};
            getTotalItems();
            document.getElementById("shoeCode").focus();
        }

        function removeItem(item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
            orderShoes();
            getTotalItems();
        }

        function onChangeCode(code) {
            dataService.findShoeContainersByCode(code).success(function (resp) {
                console.log('resp: ', resp);
                $scope.containers = resp;
                $scope.shoe.container = $scope.containers[0];
                onChangeContainer();
            }).error(function () {
                $scope.shoe = {};
                $scope.shoe.code = code;
            })
        }

        function onChangeContainer() {
            console.log('onChangeContainer: ', $scope.shoe);
            dataService.findShoeSizesByCodeAndContainer($scope.shoe.code, $scope.shoe.container)
                .success(function (resp) {
                    console.log('resp: ', resp);
                    $scope.sizes = resp;
                    $scope.shoe.size = $scope.sizes[0];
                    onChangeSize();
                })
        }

        function onChangeSize() {
            console.log('onChangeSize: ', $scope.shoe);
            dataService.getShoeByCodeContainerAndSize($scope.shoe.code, $scope.shoe.container, $scope.shoe.size)
                .success(function (resp) {
                    console.log('resp: ', resp);
                    $scope.shoe = resp;
                    updateStock();
                })
        }

        function onChangePar() {
            $scope.shoe.dozen = ($scope.shoe.par / 12).toFixed(1);
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
            getTotalItems();
            checkStock();
        }

        function onChangeDozen() {
            $scope.shoe.par = $scope.shoe.dozen * 12;
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
            getTotalItems();
            checkStock();
        }

        function onChangePrice() {
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
        }

        function processSales() {
            var sale = composeSale();
            // Add sale
            dataService.addSale(sale).then(function (saleId) {
                var saleDetails = composeSaleDetail(saleId);
                console.log('details: ', saleDetails);
                // add detail sale
                dataService.addSaleDetails(saleDetails).then(function (data) {
                    // update Shoe Stock
                    dataService.updateShoeStock(saleId).then(function (resp) {
                        console.log('updateShoeStock: ', resp);
                        $location.path('sale-detail').search({saleId: saleId});
                        setSuccessMessage("Venta Realizada Exitosamente", true);
                    });
                })
            })
        }

        function getTotalItems() {
            $scope.total = 0;
            $scope.items.forEach(function (item) {
                $scope.total = +$scope.total + +item.total;
            })
        }

        function init() {
            $scope.total = 0;
            $scope.shoe = {code: '', photo: '', container: '', size: '', par: '', dozen: '', price: '', total: ''};
            $scope.items = [];
            getCustomers();
        }

        function getCustomers() {
            dataService.getAllCustomers().success(function (resp) {
                $scope.customers = resp;
            })
        }

        function composeSaleDetail(saleId) {
            var saleDetails = [];
            $scope.items.forEach(function (shoe) {
                var detail = {
                    saleId: parseInt(saleId),
                    par: shoe.par,
                    dozen: shoe.dozen,
                    shoeId: shoe.id,
                    price: shoe.price,
                    shoeSize: shoe.size
                };
                saleDetails.push(detail);
            });
            return saleDetails;
        }

        function composeSale() {
            return {
                date: new Date(),
                customerId: $scope.customer.id,
                total: $scope.total
            };
        }

        function checkStock() {
            if ($scope.shoe.par > $scope.shoe.stockPar) {
                setWarningMessage('La Cantidad en Par no debe ser mayor al Stock Par');
                $scope.shoe.par = $scope.shoe.stockPar;
                onChangePar();
            } else if (+$scope.shoe.dozen > +$scope.shoe.stockDozen) {
                setWarningMessage('La Cantidad en Docena no debe ser mayor al Stock Docena');
                $scope.shoe.dozen = $scope.shoe.stockDozen;
                onChangeDozen();
            }
        }

        function updateStock() {
            $scope.items.forEach(function (value) {
                if (value.id === $scope.shoe.id) {
                    $scope.shoe.stockPar = +$scope.shoe.stockPar - +value.par;
                    $scope.shoe.stockDozen = (+$scope.shoe.stockDozen - +value.dozen).toFixed(1);
                }
            });
            $scope.shoe.par = $scope.shoe.stockPar;
            $scope.shoe.dozen = $scope.shoe.stockDozen;
        }

        function orderShoes() {
            $scope.items.forEach(function (value, index) {
                value.index = index;
            });
        }

        function updateExistingShoe() {
            $scope.items.forEach(function (value) {
                if (value.id === $scope.shoe.id) {
                    value.par = +value.par + +$scope.shoe.par;
                    value.dozen = +value.dozen + +$scope.shoe.dozen;
                    value.total = +value.par + +$scope.shoe.price;
                }
            });
        }

        var isShoeValid = function () {
            var flag = true;
            if (!$scope.shoe.id) {
                setWarningMessage('No puede ingresar un item vacio');
                flag = false;
            }
            if ($scope.shoe.stockPar === 0 || $scope.shoe.stockDozen === 0) {
                setWarningMessage('El Stock está en Cero');
                flag = false;
            }
            return flag;
        };

        var isShoeNotFound = function () {
            var found = $scope.items.find(function (shoe) {
                return shoe.id === $scope.shoe.id &&
                    shoe.price === $scope.shoe.price;
            });
            return found === undefined;
        }
    }
})();


