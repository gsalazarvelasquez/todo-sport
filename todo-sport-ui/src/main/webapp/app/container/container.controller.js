(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('ContainerController', ContainerController);

    function ContainerController($scope, $modal, $window, dataService) {

        init();
        $scope.selectContainer = selectContainer;
        $scope.updateContainer = updateContainer;
        $scope.addContainer = addContainer;
        $scope.deleteContainer = deleteContainer;
        $scope.cancelContainerModal = cancelContainerModal;

        function selectContainer(container) {
            $scope.container = container;
        }

        function updateContainer() {
            dataService.updateContainer($scope.container).then(function (value) {
                init();
            })
        }

        function addContainer() {
            console.log('addContainer: ', $scope.container);
            dataService.createContainer($scope.container).then(function (value) {
                console.log('createContainer: ', value);
                init();
            })
        }

        function deleteContainer() {
            var id = $scope.container.id;
            dataService.deleteContainer(id).then(function (value) {
                console.log('deleteContainer: ', value);
                init();
            })
        }

        function cancelContainerModal() {
            init();
        }

        function init() {
            $scope.container = {};
            dataService.getAllContainers().success(function (resp) {
                console.log('containers: ', resp);
                $scope.containers = resp;
            });
        }
    }
})();