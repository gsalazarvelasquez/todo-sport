(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('EditSaleController', EditSaleController);

    function EditSaleController($scope, $location, $routeParams, dataService) {

        init();
        $scope.addItem = addItem;
        $scope.editItem = editItem;
        $scope.removeItem = removeItem;
        $scope.onChangeCode = onChangeCode;
        $scope.onChangeContainer = onChangeContainer;
        $scope.onChangeSize = onChangeSize;
        $scope.onChangePar = onChangePar;
        $scope.onChangeDozen = onChangeDozen;
        $scope.onChangePrice = onChangePrice;
        $scope.onChangeCustomer = onChangeCustomer;
        $scope.updateSale = updateSale;
        $scope.enableEditCustomer = enableEditCustomer;

        function addItem() {
            if (!isShoeValid())
                return;

            if (isShoeNotFound()) {
                $scope.shoe.index = $scope.items.length;
                $scope.items.push($scope.shoe);
            } else
                updateExistingShoe();
            
            $scope.shoe = {};
            getTotalItems();
            document.getElementById("shoeCode").focus();
        }

        function editItem(item) {
            $scope.shoe = item;
        }

        function removeItem(item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
            orderShoes();
            getTotalSale();
        }

        function onChangeCode(code) {
            dataService.findShoeContainersByCode(code)
                .success(function (resp) {
                    console.log('resp: ', resp);
                    $scope.containers = resp;
                    $scope.shoe.container = $scope.containers[0];
                    onChangeContainer();
                }).error(function (e) {
                $scope.shoe = {};
                $scope.shoe.code = code;
            })
        }

        function onChangeContainer() {
            console.log('onChangeContainer: ', $scope.shoe);
            dataService.findShoeSizesByCodeAndContainer($scope.shoe.code, $scope.shoe.container)
                .success(function (resp) {
                    console.log('resp: ', resp);
                    $scope.sizes = resp;
                    $scope.shoe.size = $scope.sizes[0];
                    onChangeSize();
                })
        }

        function onChangeSize() {
            console.log('onChangeSize: ', $scope.shoe);
            dataService.getShoeByCodeContainerAndSize($scope.shoe.code, $scope.shoe.container, $scope.shoe.size)
                .success(function (resp) {
                    console.log('resp: ', resp);
                    $scope.shoe = resp;
                    updateStock();
                })
        }

        function onChangePar() {
            $scope.shoe.dozen = ($scope.shoe.par / 12).toFixed(1);
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
            getTotalItems();
            checkStock();
        }

        function onChangeDozen() {
            $scope.shoe.par = $scope.shoe.dozen * 12;
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
            getTotalItems();
            checkStock();
        }

        function onChangePrice() {
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
        }

        function onChangeCustomer() {
            $scope.isGoingToEditCustomer = false;
        }

        function updateSale() {
            var sale = composeSale();
            dataService.updateSale($scope.sale.id, sale).then(function (value) {
                console.log('updateSale: ', value);
                dataService.deleteSaleDetailBySaleId($scope.sale.id).then(function (value) {
                    console.log('deleteSaleDetailBySaleId: ', value);
                    var saleDetails = composeSaleDetail($scope.sale.id);
                    dataService.addSaleDetails(saleDetails).then(function (data) {
                        console.log('updateSaleDetails:', data);
                        $location.path('sale-detail').search({saleId: $scope.sale.id})
                        setSuccessMessage("Venta Actualizada Exitosamente", true);
                    })
                });
            })
        }

        function enableEditCustomer() {
            $scope.isGoingToEditCustomer = true;
        }

        function init() {
            $scope.isGoingToEditCustomer = false;
            $scope.shoe = {code: '', container: '', size: '', par: '', dozen: '', price: '', total: ''};
            $scope.items = [];
            var saleId = $routeParams.saleId;
            getSaleById(saleId);
            getCustomers();
        }

        function getCustomers() {
            dataService.getAllCustomers().success(function (resp) {
                console.log('getAllCustomers: ', resp);
                $scope.customers = resp;
            })
        }

        function getSaleById(saleId) {
            dataService.getSaleById(saleId).success(function (resp) {
                console.log('getSaleById: ', resp);
                $scope.sale = resp;
                getSalesDetails($scope.sale.id);
            })
        }

        function composeSaleDetail(saleId) {
            var saleDetails = [];
            $scope.items.forEach(function (shoe) {
                var detail = {
                    saleId: parseInt(saleId),
                    par: shoe.par,
                    dozen: shoe.dozen,
                    shoeId: shoe.shoeId ? shoe.shoeId : shoe.id,
                    price: shoe.price,
                    shoeSize: shoe.size
                };
                saleDetails.push(detail);
            });
            return saleDetails;
        }

        function getSalesDetails(saleId) {
            dataService.findSaleDetailBySaleId(saleId).success(function (data) {
                console.log('findSaleDetailBySaleId: ', data);
                composeItems(data);
                getTotalSale();
            });
        }

        function composeItems(saleDetails) {
            saleDetails.forEach(function (detail, index) {
                $scope.shoe = {code: '', container: '', size: '', par: '', dozen: '', price: '', total: ''};
                var item = {
                    id: detail.id,
                    code: detail.shoe.code,
                    index: index,
                    shoeId: detail.shoe.id,
                    container: detail.shoe.container,
                    size: detail.shoeSize,
                    par: detail.par,
                    dozen: detail.dozen,
                    price: detail.price,
                    total: +detail.price * +detail.par
                };
                $scope.items.push(item);
            })

        }

        function getTotalSale() {
            $scope.sale.total = 0;
            $scope.items.forEach(function (item) {
                $scope.sale.total += parseInt(item.total);
            })
        }

        function composeSale() {
            return {
                customerId: $scope.sale.customer.id,
                total: $scope.sale.total
            };
        }

        function updateStock() {
            $scope.items.forEach(function (value) {
                if (value.shoeId === $scope.shoe.id) {
                    $scope.shoe.stockPar = +$scope.shoe.stockPar - +value.par;
                    $scope.shoe.stockDozen = (+$scope.shoe.stockDozen - +value.dozen).toFixed(1);
                }
            });
            $scope.shoe.par = $scope.shoe.stockPar;
            $scope.shoe.dozen = $scope.shoe.stockDozen;
        }

        function getTotalItems() {
            $scope.total = 0;
            $scope.items.forEach(function (item) {
                $scope.total = +$scope.total + +item.total;
            })
        }

        function checkStock() {
            console.log('dozen: ', $scope.shoe.dozen);
            if ($scope.shoe.par > $scope.shoe.stockPar) {
                setWarningMessage('La Cantidad en Par no debe ser mayor al Stock Par');
                $scope.shoe.par = $scope.shoe.stockPar;
                onChangePar();
            } else if (+$scope.shoe.dozen > +$scope.shoe.stockDozen) {
                setWarningMessage('La Cantidad en Docena no debe ser mayor al Stock Docena');
                $scope.shoe.dozen = $scope.shoe.stockDozen;
                onChangeDozen();
            }
        }

        // Private function
        function orderShoes() {
            $scope.items.forEach(function (value, index) {
                value.index = index;
            });
        }

        function updateExistingShoe() {
            $scope.items.forEach(function (value) {
                if (value.shoeId === $scope.shoe.id) {
                    value.par = +value.par + +$scope.shoe.par;
                    value.dozen = +value.dozen + +$scope.shoe.dozen;
                    value.total = +value.par + +$scope.shoe.price;
                }
            });
        }

        var isShoeValid = function () {
            var flag = true;
            if (!$scope.shoe.id) {
                setWarningMessage('No puede ingresar un item vacio');
                flag = false;
            }
            if ($scope.shoe.stockPar === 0 || $scope.shoe.stockDozen === 0) {
                setWarningMessage('El Stock está en Cero');
                flag = false;
            }
            return flag;
        };

        var isShoeNotFound = function () {
            var found = $scope.items.find(function (shoe) {
                return shoe.shoeId === $scope.shoe.id &&
                    shoe.price === $scope.shoe.price;
            });
            return found === undefined;
        }
    }
})();


