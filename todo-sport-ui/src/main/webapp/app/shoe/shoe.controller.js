(function () {

    'use strict';

    angular
        .module('todo-sport')
        .controller('ShoeController', ShoeController);

    function ShoeController($scope, $modal, $window, dataService) {

        init();
        $scope.selectShoe = selectShoe;
        $scope.onChangePar = onChangePar;
        $scope.onChangeDozen = onChangeDozen;
        $scope.onChangePrice = onChangePrice;
        $scope.cancelShoeModal = cancelShoeModal;
        $scope.deleteShoe = deleteShoe;
        $scope.updateShoe = updateShoe;
        $scope.importExcelShoe = importExcelShoe;


        function selectShoe(shoe) {
            $scope.shoe = shoe;
        }

        function onChangePar() {
            $scope.shoe.dozen = ($scope.shoe.par / 12).toFixed(1);
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
        }

        function onChangeDozen() {
            $scope.shoe.par = $scope.shoe.dozen * 12;
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
        }

        function onChangePrice() {
            $scope.shoe.total = $scope.shoe.par * $scope.shoe.price;
        }

        function cancelShoeModal() {
            init();
        }

        function deleteShoe() {
            var id = $scope.shoe.id;
            dataService.deleteShoe(id).then(function (value) {
                init();
            })
        }

        function updateShoe() {
            console.log('updateShoe: ', $scope.shoe);
            dataService.updateShoe($scope.shoe).then(function (value) {
                console.log('res: ', value);
                updateShoePhoto();
            });
        }

        function importExcelShoe() {
            var file = document.getElementById("excelFile");
            var excelFile = file.files[0];
            console.log('excelFile', excelFile);
            if (!excelFile) return;

            var data = new FormData();
            data.append('excelFile', excelFile);

            dataService.importExcelShoe(data).then(function (value) {
                console.log('import Excel File: ', value);
                init();
            })
        }

        function init() {
            dataService.getAllCustomers().success(function (resp) {
                console.log('customers: ', resp);
                $scope.customers = resp;
            });
            dataService.getAllShoes().success(function (resp) {
                $scope.shoes = resp;
            });

            dataService.getAllContainers().success(function (resp) {
                $scope.containers = resp;
            });

            dataService.getAllShoeSizes().success(function (resp) {
                $scope.shoeSizes = resp;
            });
        }

        function updateShoePhoto() {
            var shoePhoto = document.getElementById("image");
            var image = shoePhoto.files[0];
            console.log('image', image);
            if (!image) return;

            var data = new FormData();
            data.append('image', image);
            data.append('fileType', image.type);

            dataService.uploadPhoto($scope.shoe.id, data).then(function (value) {
                console.log('upload Photo: ', value);
                init();
            })
        }
    }
})();